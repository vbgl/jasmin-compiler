open BinNums
open Datatypes
open Allocation
open Compiler_util
open Expr
open Seq
open Type
open Utils0
open Var0

val get_flag : (Var.var -> bool) -> lval -> assgn_tag -> assgn_tag

val assgn_tuple :
  (Var.var -> bool) -> instr_info -> lval list -> assgn_tag -> stype list ->
  pexpr list -> instr list

val inline_c :
  (instr -> Sv.t -> (Sv.t * instr list) ciexec) -> instr list -> Sv.t ->
  (instr_info * error_msg, Sv.t * instr list) result

val sparams : fundef -> Sv.t

val locals_p : fundef -> Sv.t

val locals : fundef -> Sv.t

val check_rename :
  instr_info -> funname -> fundef -> fundef -> Sv.t ->
  (instr_info * error_msg, unit) result

val get_fun : fun_decl list -> instr_info -> funname -> fundef ciexec

val dummy_info : positive

val mkdV : Var.var -> var_i

val arr_init : positive -> pexpr

val array_init : instr_info -> Sv.t -> instr list

val inline_i :
  (Var.var -> bool) -> (instr_info -> funname -> fundef -> fundef) ->
  fun_decl list -> instr -> Sv.t -> (Sv.t * instr list) ciexec

val inline_fd :
  (Var.var -> bool) -> (instr_info -> funname -> fundef -> fundef) ->
  fun_decl list -> fundef -> (instr_info * error_msg, fundef) result

val inline_fd_cons :
  (Var.var -> bool) -> (instr_info -> funname -> fundef -> fundef) ->
  (funname * fundef) -> fun_decl list cfexec -> (fun_error,
  (funname * fundef) list) result

val inline_prog :
  (Var.var -> bool) -> (instr_info -> funname -> fundef -> fundef) ->
  fun_decl list -> fun_decl list cfexec

val inline_prog_err :
  (Var.var -> bool) -> (instr_info -> funname -> fundef -> fundef) -> prog ->
  (fun_error, prog) result

val is_array_init : pexpr -> bool

val remove_init_i : instr -> instr list

val remove_init_c : instr list -> instr list

val remove_init_fd : fundef -> fundef

val remove_init_prog : prog -> prog
