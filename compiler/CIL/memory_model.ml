open BinInt
open BinNums
open Bool
open Eqtype
open Seq
open Ssralg
open Utils0
open Word0
open Wsize

module LE =
 struct
  (** val encode : wsize -> GRing.ComRing.sort -> GRing.ComRing.sort list **)

  let encode sz w =
    Obj.magic split_vec sz (nat_of_wsize U8) w

  (** val decode : wsize -> GRing.ComRing.sort list -> GRing.ComRing.sort **)

  let decode sz n =
    make_vec U8 sz n

  (** val wread8 :
      wsize -> GRing.ComRing.sort -> coq_Z -> GRing.ComRing.sort **)

  let wread8 ws v k =
    nth (wrepr U8 Z0) (encode ws v) (Z.to_nat k)
 end

type 'core_mem coreMem = { add : (Equality.sort -> coq_Z -> Equality.sort);
                           sub : (Equality.sort -> Equality.sort -> coq_Z);
                           uget : ('core_mem -> Equality.sort ->
                                  GRing.ComRing.sort);
                           uset : ('core_mem -> Equality.sort ->
                                  GRing.ComRing.sort -> 'core_mem);
                           validr : ('core_mem -> Equality.sort -> wsize ->
                                    unit exec);
                           validw : ('core_mem -> Equality.sort -> wsize ->
                                    unit exec) }

(** val add :
    Equality.coq_type -> 'a1 coreMem -> Equality.sort -> coq_Z ->
    Equality.sort **)

let add _ x = x.add

(** val uget :
    Equality.coq_type -> 'a1 coreMem -> 'a1 -> Equality.sort ->
    GRing.ComRing.sort **)

let uget _ x = x.uget

(** val uset :
    Equality.coq_type -> 'a1 coreMem -> 'a1 -> Equality.sort ->
    GRing.ComRing.sort -> 'a1 **)

let uset _ x = x.uset

(** val validr :
    Equality.coq_type -> 'a1 coreMem -> 'a1 -> Equality.sort -> wsize -> unit
    exec **)

let validr _ x = x.validr

(** val validw :
    Equality.coq_type -> 'a1 coreMem -> 'a1 -> Equality.sort -> wsize -> unit
    exec **)

let validw _ x = x.validw

module CoreMem =
 struct
  (** val uread :
      Equality.coq_type -> 'a1 coreMem -> 'a1 -> Equality.sort -> coq_Z ->
      GRing.ComRing.sort list **)

  let uread _ cM m ptr n =
    map (fun o -> cM.uget m (cM.add ptr o)) (ziota Z0 n)

  (** val read :
      Equality.coq_type -> 'a1 coreMem -> 'a1 -> Equality.sort -> wsize ->
      GRing.ComRing.sort exec **)

  let read pointer cM m ptr sz =
    Result.bind (fun _ -> Ok
      (LE.decode sz (uread pointer cM m ptr (wsize_size sz))))
      (cM.validr m ptr sz)

  (** val uwrite :
      Equality.coq_type -> 'a1 coreMem -> 'a1 -> Equality.sort -> wsize ->
      GRing.ComRing.sort -> 'a1 **)

  let uwrite _ cM m ptr sz w =
    foldl (fun m0 o -> cM.uset m0 (cM.add ptr o) (LE.wread8 sz w o)) m
      (ziota Z0 (wsize_size sz))

  (** val write :
      Equality.coq_type -> 'a1 coreMem -> 'a1 -> Equality.sort -> wsize ->
      GRing.ComRing.sort -> 'a1 exec **)

  let write pointer cM m ptr sz w =
    Result.bind (fun _ -> Ok (uwrite pointer cM m ptr sz w))
      (cM.validw m ptr sz)
 end

type alignment =
  GRing.ComRing.sort -> wsize -> bool
  (* singleton inductive, whose constructor was Alignment *)

(** val is_align : alignment -> GRing.ComRing.sort -> wsize -> bool **)

let is_align alignment0 =
  alignment0

type 'mem memory = { read_mem : ('mem -> GRing.ComRing.sort -> wsize ->
                                GRing.ComRing.sort exec);
                     write_mem : ('mem -> GRing.ComRing.sort -> wsize ->
                                 GRing.ComRing.sort -> 'mem exec);
                     valid_pointer : ('mem -> GRing.ComRing.sort -> wsize ->
                                     bool);
                     stack_root : ('mem -> GRing.ComRing.sort);
                     frames : ('mem -> (GRing.ComRing.sort * coq_Z) list);
                     alloc_stack : ('mem -> coq_Z -> 'mem exec);
                     free_stack : ('mem -> coq_Z -> 'mem);
                     init : ((GRing.ComRing.sort * coq_Z) list ->
                            GRing.ComRing.sort -> 'mem exec) }

(** val read_mem :
    'a1 memory -> 'a1 -> GRing.ComRing.sort -> wsize -> GRing.ComRing.sort
    exec **)

let read_mem x = x.read_mem

(** val write_mem :
    'a1 memory -> 'a1 -> GRing.ComRing.sort -> wsize -> GRing.ComRing.sort ->
    'a1 exec **)

let write_mem x = x.write_mem

module type MemoryT =
 sig
  val coq_A : alignment

  type mem

  val coq_CM : mem coreMem

  val coq_M : mem memory

  val readV : mem -> GRing.ComRing.sort -> wsize -> reflect

  val writeV :
    mem -> GRing.ComRing.sort -> wsize -> GRing.ComRing.sort -> reflect

  val valid_pointerP : mem -> GRing.ComRing.sort -> wsize -> reflect
 end
