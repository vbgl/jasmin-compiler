open Datatypes
open Compiler_util
open Eqtype
open Expr
open Seq
open Type
open Utils0
open Var0
open X86_instr_decl

(** val dead_code_c :
    (instr -> Sv.t -> (Sv.t * instr list) ciexec) -> instr list -> Sv.t ->
    (Sv.t * instr list) ciexec **)

let dead_code_c dead_code_i0 c s =
  foldr (fun i r ->
    Result.bind (fun r0 ->
      Result.bind (fun ri -> ciok ((fst ri), (cat (snd ri) (snd r0))))
        (dead_code_i0 i (fst r0))) r) (ciok (s, [])) c

(** val loop :
    (Sv.t -> (Sv.t * instr list) ciexec) -> instr_info -> nat -> Sv.t -> Sv.t
    -> Sv.t -> (Sv.t * instr list) ciexec **)

let rec loop dead_code_c0 ii n rx wx s =
  match n with
  | O ->
    cierror ii (Cerr_Loop
      ('d'::('e'::('a'::('d'::('_'::('c'::('o'::('d'::('e'::[]))))))))))
  | S n0 ->
    Result.bind (fun sc ->
      let (s', c') = sc in
      let s'0 = Sv.union rx (Sv.diff s' wx) in
      if Sv.subset s'0 s
      then ciok (s, c')
      else loop dead_code_c0 ii n0 rx wx (Sv.union s s'0)) (dead_code_c0 s)

(** val wloop :
    (Sv.t -> (Sv.t * (Sv.t * (instr list * instr list))) ciexec) ->
    instr_info -> nat -> Sv.t -> (Sv.t * (instr list * instr list)) ciexec **)

let rec wloop dead_code_c2 ii n s =
  match n with
  | O ->
    cierror ii (Cerr_Loop
      ('d'::('e'::('a'::('d'::('_'::('c'::('o'::('d'::('e'::[]))))))))))
  | S n0 ->
    Result.bind (fun sc ->
      let (s', sic) = sc in
      if Sv.subset s' s
      then ciok sic
      else wloop dead_code_c2 ii n0 (Sv.union s s')) (dead_code_c2 s)

(** val write_mem : lval -> bool **)

let write_mem = function
| Lmem (_, _, _) -> true
| _ -> false

(** val check_nop : lval -> Equality.sort -> pexpr -> bool **)

let check_nop rv ty e =
  match rv with
  | Lvar x1 ->
    (match e with
     | Pvar x2 ->
       (&&) (eq_op Var.var_eqType (Obj.magic v_var x1) (Obj.magic v_var x2))
         (eq_op stype_eqType ty (Obj.magic Var.vtype x1.v_var))
     | _ -> false)
  | _ -> false

(** val check_nop_opn : lval list -> sopn -> pexpr list -> bool **)

let check_nop_opn xs o es =
  match xs with
  | [] -> false
  | x :: l ->
    (match l with
     | [] ->
       (match o with
        | Ox86 a ->
          (match a with
           | MOV sz ->
             (match es with
              | [] -> false
              | e :: l0 ->
                (match l0 with
                 | [] -> check_nop x (Obj.magic (Coq_sword sz)) e
                 | _ :: _ -> false))
           | _ -> false)
        | _ -> false)
     | _ :: _ -> false)

(** val dead_code_i : instr -> Sv.t -> (Sv.t * instr list) ciexec **)

let rec dead_code_i i s =
  let MkI (ii, ir) = i in
  (match ir with
   | Cassgn (x, tag, ty, e) ->
     let w = write_i ir in
     if negb (eq_op assgn_tag_eqType (Obj.magic tag) (Obj.magic AT_keep))
     then if (&&) (disjoint s w) (negb (write_mem x))
          then ciok (s, [])
          else if check_nop x (Obj.magic ty) e
               then ciok (s, [])
               else ciok ((read_rv_rec (read_e_rec (Sv.diff s w) e) x),
                      (i :: []))
     else ciok ((read_rv_rec (read_e_rec (Sv.diff s w) e) x), (i :: []))
   | Copn (xs, tag, o, es) ->
     let w = vrvs xs in
     if negb (eq_op assgn_tag_eqType (Obj.magic tag) (Obj.magic AT_keep))
     then if (&&) (disjoint s w) (negb (has write_mem xs))
          then ciok (s, [])
          else if check_nop_opn xs o es
               then ciok (s, [])
               else ciok
                      ((read_es_rec (read_rvs_rec (Sv.diff s (vrvs xs)) xs)
                         es), (i :: []))
     else ciok ((read_es_rec (read_rvs_rec (Sv.diff s (vrvs xs)) xs) es),
            (i :: []))
   | Cif (b, c1, c2) ->
     Result.bind (fun sc1 ->
       Result.bind (fun sc2 ->
         let (s1, c3) = sc1 in
         let (s2, c4) = sc2 in
         ciok ((read_e_rec (Sv.union s1 s2) b), ((MkI (ii, (Cif (b, c3,
           c4)))) :: []))) (dead_code_c dead_code_i c2 s))
       (dead_code_c dead_code_i c1 s)
   | Cfor (x, r, c) ->
     let (p, e2) = r in
     let (dir, e1) = p in
     Result.bind (fun sc ->
       let (s0, c0) = sc in
       ciok ((read_e_rec (read_e_rec s0 e2) e1), ((MkI (ii, (Cfor (x, ((dir,
         e1), e2), c0)))) :: [])))
       (loop (dead_code_c dead_code_i c) ii Loop.nb (read_rv (Lvar x))
         (vrv (Lvar x)) s)
   | Cwhile (a, c, e, c') ->
     let dobody = fun s_o ->
       let s_o' = read_e_rec s_o e in
       Result.bind (fun sci ->
         let (s_i, c0) = sci in
         Result.bind (fun sci' ->
           let (s_i', c'0) = sci' in Ok (s_i', (s_i, (c0, c'0))))
           (dead_code_c dead_code_i c' s_i)) (dead_code_c dead_code_i c s_o')
     in
     Result.bind (fun sc ->
       let (s0, y) = sc in
       let (c0, c'0) = y in
       ciok (s0, ((MkI (ii, (Cwhile (a, c0, e, c'0)))) :: [])))
       (wloop dobody ii Loop.nb s)
   | Ccall (_, xs, _, es) ->
     ciok ((read_es_rec (read_rvs_rec (Sv.diff s (vrvs xs)) xs) es),
       (i :: [])))

(** val dead_code_fd : fundef -> (instr_info * error_msg, fundef) result **)

let dead_code_fd fd =
  let { f_iinfo = ii; f_tyin = tyi; f_params = params; f_body = c; f_tyout =
    tyo; f_res = res } = fd
  in
  let s = read_es (map (fun x -> Pvar x) res) in
  Result.bind (fun c0 ->
    ciok { f_iinfo = ii; f_tyin = tyi; f_params = params; f_body = (snd c0);
      f_tyout = tyo; f_res = res }) (dead_code_c dead_code_i c s)

(** val dead_code_prog : prog -> prog cfexec **)

let dead_code_prog p =
  Result.bind (fun funcs -> Ok { p_globs = p.p_globs; p_funcs = funcs })
    (map_cfprog dead_code_fd p.p_funcs)
