open Bool
open Datatypes
open Eqtype
open Gen_map
open Ssrbool
open Utils0

type __ = Obj.t

(** val bool_beq : bool -> bool -> bool **)

let rec bool_beq x y =
  if x then y else if y then false else true

(** val ascii_beq : char -> char -> bool **)

let rec ascii_beq x y =
  (* If this appears, you're using Ascii internals. Please don't *)
 (fun f c ->
  let n = Char.code c in
  let h i = (n land (1 lsl i)) <> 0 in
  f (h 0) (h 1) (h 2) (h 3) (h 4) (h 5) (h 6) (h 7))
    (fun x0 x1 x2 x3 x4 x5 x6 x7 ->
    (* If this appears, you're using Ascii internals. Please don't *)
 (fun f c ->
  let n = Char.code c in
  let h i = (n land (1 lsl i)) <> 0 in
  f (h 0) (h 1) (h 2) (h 3) (h 4) (h 5) (h 6) (h 7))
      (fun x8 x9 x10 x11 x12 x13 x14 x15 ->
      (&&) (bool_beq x0 x8)
        ((&&) (bool_beq x1 x9)
          ((&&) (bool_beq x2 x10)
            ((&&) (bool_beq x3 x11)
              ((&&) (bool_beq x4 x12)
                ((&&) (bool_beq x5 x13)
                  ((&&) (bool_beq x6 x14) (bool_beq x7 x15))))))))
      y)
    x

(** val ascii_cmp : char -> char -> comparison **)

let ascii_cmp c c' =
  (* If this appears, you're using Ascii internals. Please don't *)
 (fun f c ->
  let n = Char.code c in
  let h i = (n land (1 lsl i)) <> 0 in
  f (h 0) (h 1) (h 2) (h 3) (h 4) (h 5) (h 6) (h 7))
    (fun b1 b2 b3 b4 b5 b6 b7 b8 ->
    (* If this appears, you're using Ascii internals. Please don't *)
 (fun f c ->
  let n = Char.code c in
  let h i = (n land (1 lsl i)) <> 0 in
  f (h 0) (h 1) (h 2) (h 3) (h 4) (h 5) (h 6) (h 7))
      (fun b1' b2' b3' b4' b5' b6' b7' b8' ->
      match bool_cmp b1 b1' with
      | Eq ->
        (match bool_cmp b2 b2' with
         | Eq ->
           (match bool_cmp b3 b3' with
            | Eq ->
              (match bool_cmp b4 b4' with
               | Eq ->
                 (match bool_cmp b5 b5' with
                  | Eq ->
                    (match bool_cmp b6 b6' with
                     | Eq ->
                       (match bool_cmp b7 b7' with
                        | Eq -> bool_cmp b8 b8'
                        | x -> x)
                     | x -> x)
                  | x -> x)
               | x -> x)
            | x -> x)
         | x -> x)
      | x -> x)
      c')
    c

(** val string_beq : char list -> char list -> bool **)

let rec string_beq x y =
  match x with
  | [] -> (match y with
           | [] -> true
           | _::_ -> false)
  | x0::x1 ->
    (match y with
     | [] -> false
     | x2::x3 -> (&&) (ascii_beq x0 x2) (string_beq x1 x3))

(** val string_eqP : char list Equality.axiom **)

let string_eqP x y =
  iffP (string_beq x y) (idP (string_beq x y))

(** val string_eqMixin : char list Equality.mixin_of **)

let string_eqMixin =
  { Equality.op = string_beq; Equality.mixin_of__1 = string_eqP }

(** val string_eqType : Equality.coq_type **)

let string_eqType =
  Obj.magic string_eqMixin

(** val string_cmp : char list -> char list -> comparison **)

let rec string_cmp s1 s2 =
  match s1 with
  | [] -> (match s2 with
           | [] -> Eq
           | _::_ -> Lt)
  | c1::s3 ->
    (match s2 with
     | [] -> Gt
     | c2::s4 -> (match ascii_cmp c1 c2 with
                  | Eq -> string_cmp s3 s4
                  | x -> x))

module CmpString =
 struct
  (** val t : Equality.coq_type **)

  let t =
    Equality.clone string_eqType (Obj.magic string_eqMixin) (fun x -> x)

  (** val cmp : Equality.sort -> Equality.sort -> comparison **)

  let cmp =
    Obj.magic string_cmp
 end

module Ms = Mmake(CmpString)
