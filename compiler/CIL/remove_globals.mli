open BinNums
open Datatypes
open Compiler_util
open Eqtype
open Expr
open Global
open Seq
open SsrZ
open Type
open Utils0
open Var0
open Wsize

val myfind : ('a1 -> 'a2 option) -> 'a1 list -> 'a2 option

val find_glob :
  instr_info -> var_i -> glob_decl list -> wsize -> coq_Z -> (fun_error,
  global) result

val add_glob :
  (glob_decl list -> Var.var -> Equality.sort) -> instr_info -> Var.var ->
  glob_decl list -> wsize -> coq_Z -> (fun_error, glob_decl list) result

val extend_glob_i :
  (Var.var -> bool) -> (glob_decl list -> Var.var -> Equality.sort) -> instr
  -> glob_decl list -> (fun_error, glob_decl list) result

val extend_glob_prog :
  (Var.var -> bool) -> (glob_decl list -> Var.var -> Equality.sort) -> prog
  -> (fun_error, glob_decl list) result

val remove_glob_e :
  (Var.var -> bool) -> instr_info -> global Mvar.t -> pexpr -> (fun_error,
  pexpr) result

val remove_glob_lv :
  (Var.var -> bool) -> instr_info -> global Mvar.t -> lval -> (fun_error,
  lval) result

val remove_glob :
  (global Mvar.t -> instr -> (global Mvar.t * instr list) cfexec) -> global
  Mvar.t -> instr list -> (global Mvar.t * instr list) cfexec

val merge_glob : Var.var -> global option -> global option -> global option

val coq_Mincl : global Mvar.t -> global Mvar.t -> bool

val merge_env : global Mvar.t -> global Mvar.t -> global Mvar.t

val loop :
  funname -> (global Mvar.t -> (global Mvar.t * instr list) cfexec) -> nat ->
  global Mvar.t -> (global Mvar.t * instr list) cfexec

type check2_r =
| Check2_r of pexpr * (global Mvar.t * instr list)
   * (global Mvar.t * instr list)

type loop2_r =
| Loop2_r of pexpr * instr list * instr list * global Mvar.t

val loop2 :
  funname -> (global Mvar.t -> check2_r cfexec) -> nat -> global Mvar.t ->
  loop2_r cfexec

val remove_glob_i :
  (Var.var -> bool) -> glob_decl list -> funname -> global Mvar.t -> instr ->
  (global Mvar.t * instr list) cfexec

val remove_glob_fundef :
  (Var.var -> bool) -> glob_decl list -> (funname * fundef) -> (fun_error,
  funname * fundef) result

val remove_glob_prog :
  (Var.var -> bool) -> (glob_decl list -> Var.var -> Equality.sort) -> prog
  -> (fun_error, prog) result
