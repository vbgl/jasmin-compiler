open BinNums
open Datatypes
open Allocation
open Compiler_util
open Expr
open Seq
open Type
open Utils0
open Var0

(** val get_flag : (Var.var -> bool) -> lval -> assgn_tag -> assgn_tag **)

let get_flag inline_var x flag =
  match x with
  | Lvar x0 -> if inline_var x0.v_var then AT_inline else flag
  | _ -> flag

(** val assgn_tuple :
    (Var.var -> bool) -> instr_info -> lval list -> assgn_tag -> stype list
    -> pexpr list -> instr list **)

let assgn_tuple inline_var iinfo xs flag tys es =
  let assgn = fun xe -> MkI (iinfo, (Cassgn ((fst xe),
    (get_flag inline_var (fst xe) flag), (fst (snd xe)), (snd (snd xe)))))
  in
  map assgn (zip xs (zip tys es))

(** val inline_c :
    (instr -> Sv.t -> (Sv.t * instr list) ciexec) -> instr list -> Sv.t ->
    (instr_info * error_msg, Sv.t * instr list) result **)

let inline_c inline_i0 c s =
  foldr (fun i r ->
    Result.bind (fun r0 ->
      Result.bind (fun ri -> ciok ((fst ri), (cat (snd ri) (snd r0))))
        (inline_i0 i (fst r0))) r) (ciok (s, [])) c

(** val sparams : fundef -> Sv.t **)

let sparams fd =
  vrvs_rec Sv.empty (map (fun x -> Lvar x) fd.f_params)

(** val locals_p : fundef -> Sv.t **)

let locals_p fd =
  let s = read_es (map (fun x -> Pvar x) fd.f_res) in
  let w = write_c_rec s fd.f_body in
  let r = read_c_rec w fd.f_body in
  vrvs_rec r (map (fun x -> Lvar x) fd.f_params)

(** val locals : fundef -> Sv.t **)

let locals fd =
  Sv.diff (locals_p fd) (sparams fd)

(** val check_rename :
    instr_info -> funname -> fundef -> fundef -> Sv.t ->
    (instr_info * error_msg, unit) result **)

let check_rename iinfo f fd1 fd2 s =
  Result.bind (fun _ ->
    let s2 = locals_p fd2 in
    if disjoint s s2 then ciok () else cierror iinfo (Cerr_inline (s, s2)))
    (add_infun iinfo (CheckAllocReg.check_fundef (f, fd1) (f, fd2) ()))

(** val get_fun : fun_decl list -> instr_info -> funname -> fundef ciexec **)

let get_fun p iinfo f =
  match get_fundef p f with
  | Some fd -> ciok fd
  | None ->
    cierror iinfo (Cerr_unknown_fun (f,
      ('i'::('n'::('l'::('i'::('n'::('i'::('n'::('g'::[]))))))))))

(** val dummy_info : positive **)

let dummy_info =
  Coq_xH

(** val mkdV : Var.var -> var_i **)

let mkdV x =
  { v_var = x; v_info = dummy_info }

(** val arr_init : positive -> pexpr **)

let arr_init p =
  Parr_init p

(** val array_init : instr_info -> Sv.t -> instr list **)

let array_init iinfo x =
  let assgn = fun x0 c ->
    match x0.Var.vtype with
    | Coq_sarr p ->
      (MkI (iinfo, (Cassgn ((Lvar (mkdV x0)), AT_rename, x0.Var.vtype,
        (arr_init p))))) :: c
    | _ -> c
  in
  Sv.fold (Obj.magic assgn) x []

(** val inline_i :
    (Var.var -> bool) -> (instr_info -> funname -> fundef -> fundef) ->
    fun_decl list -> instr -> Sv.t -> (Sv.t * instr list) ciexec **)

let rec inline_i inline_var rename_fd p i x =
  let MkI (iinfo, ir) = i in
  (match ir with
   | Cif (e, c1, c2) ->
     Result.bind (fun c3 ->
       Result.bind (fun c4 ->
         ciok ((read_e_rec (Sv.union (fst c3) (fst c4)) e), ((MkI (iinfo,
           (Cif (e, (snd c3), (snd c4))))) :: [])))
         (inline_c (inline_i inline_var rename_fd p) c2 x))
       (inline_c (inline_i inline_var rename_fd p) c1 x)
   | Cfor (x0, r, c) ->
     let x1 = Sv.union (read_i ir) x in
     Result.bind (fun c0 ->
       ciok (x1, ((MkI (iinfo, (Cfor (x0, r, (snd c0))))) :: [])))
       (inline_c (inline_i inline_var rename_fd p) c x1)
   | Cwhile (a, c, e, c') ->
     let x0 = Sv.union (read_i ir) x in
     Result.bind (fun c0 ->
       Result.bind (fun c'0 ->
         ciok (x0, ((MkI (iinfo, (Cwhile (a, (snd c0), e,
           (snd c'0))))) :: [])))
         (inline_c (inline_i inline_var rename_fd p) c' x0))
       (inline_c (inline_i inline_var rename_fd p) c x0)
   | Ccall (inline, xs, f, es) ->
     let x0 = Sv.union (read_i ir) x in
     (match inline with
      | InlineFun ->
        Result.bind (fun fd ->
          let fd' = rename_fd iinfo f fd in
          Result.bind (fun _ ->
            let init_array = array_init iinfo (locals fd') in
            ciok (x0,
              (cat
                (assgn_tuple inline_var iinfo
                  (map (fun x1 -> Lvar x1) fd'.f_params) AT_rename fd'.f_tyin
                  es)
                (cat init_array
                  (cat fd'.f_body
                    (assgn_tuple inline_var iinfo xs AT_rename fd'.f_tyout
                      (map (fun x1 -> Pvar x1) fd'.f_res)))))))
            (check_rename iinfo f fd fd' (Sv.union (vrvs xs) x0)))
          (get_fun p iinfo f)
      | DoNotInline -> ciok (x0, (i :: [])))
   | _ -> ciok ((Sv.union (read_i ir) x), (i :: [])))

(** val inline_fd :
    (Var.var -> bool) -> (instr_info -> funname -> fundef -> fundef) ->
    fun_decl list -> fundef -> (instr_info * error_msg, fundef) result **)

let inline_fd inline_var rename_fd p fd =
  let { f_iinfo = ii; f_tyin = tyin; f_params = params; f_body = c; f_tyout =
    tyout; f_res = res } = fd
  in
  let s = read_es (map (fun x -> Pvar x) res) in
  Result.bind (fun c0 -> Ok { f_iinfo = ii; f_tyin = tyin; f_params = params;
    f_body = (snd c0); f_tyout = tyout; f_res = res })
    (inline_c (inline_i inline_var rename_fd p) c s)

(** val inline_fd_cons :
    (Var.var -> bool) -> (instr_info -> funname -> fundef -> fundef) ->
    (funname * fundef) -> fun_decl list cfexec -> (fun_error,
    (funname * fundef) list) result **)

let inline_fd_cons inline_var rename_fd ffd p =
  Result.bind (fun p0 ->
    let f = fst ffd in
    Result.bind (fun fd -> cfok ((f, fd) :: p0))
      (add_finfo f f (inline_fd inline_var rename_fd p0 (snd ffd)))) p

(** val inline_prog :
    (Var.var -> bool) -> (instr_info -> funname -> fundef -> fundef) ->
    fun_decl list -> fun_decl list cfexec **)

let inline_prog inline_var rename_fd p =
  foldr (inline_fd_cons inline_var rename_fd) (cfok []) p

(** val inline_prog_err :
    (Var.var -> bool) -> (instr_info -> funname -> fundef -> fundef) -> prog
    -> (fun_error, prog) result **)

let inline_prog_err inline_var rename_fd p =
  if uniq pos_eqType (map (fun x -> fst (Obj.magic x)) p.p_funcs)
  then Result.bind (fun fds -> Ok { p_globs = p.p_globs; p_funcs = fds })
         (inline_prog inline_var rename_fd p.p_funcs)
  else cferror Ferr_uniqfun

(** val is_array_init : pexpr -> bool **)

let is_array_init = function
| Parr_init _ -> true
| _ -> false

(** val remove_init_i : instr -> instr list **)

let rec remove_init_i i = match i with
| MkI (ii, ir) ->
  (match ir with
   | Cassgn (_, _, _, e) -> if is_array_init e then [] else i :: []
   | Cif (e, c1, c2) ->
     let c3 = foldr (fun i0 c -> cat (remove_init_i i0) c) [] c1 in
     let c4 = foldr (fun i0 c -> cat (remove_init_i i0) c) [] c2 in
     (MkI (ii, (Cif (e, c3, c4)))) :: []
   | Cfor (x, r, c) ->
     let c0 = foldr (fun i0 c0 -> cat (remove_init_i i0) c0) [] c in
     (MkI (ii, (Cfor (x, r, c0)))) :: []
   | Cwhile (a, c, e, c') ->
     let c0 = foldr (fun i0 c0 -> cat (remove_init_i i0) c0) [] c in
     let c'0 = foldr (fun i0 c1 -> cat (remove_init_i i0) c1) [] c' in
     (MkI (ii, (Cwhile (a, c0, e, c'0)))) :: []
   | _ -> i :: [])

(** val remove_init_c : instr list -> instr list **)

let remove_init_c c =
  foldr (fun i c0 -> cat (remove_init_i i) c0) [] c

(** val remove_init_fd : fundef -> fundef **)

let remove_init_fd fd =
  { f_iinfo = fd.f_iinfo; f_tyin = fd.f_tyin; f_params = fd.f_params;
    f_body = (remove_init_c fd.f_body); f_tyout = fd.f_tyout; f_res =
    fd.f_res }

(** val remove_init_prog : prog -> prog **)

let remove_init_prog p =
  map_prog remove_init_fd p
