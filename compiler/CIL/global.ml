open BinNums
open Bool
open Eqtype
open Ssralg
open Ssrbool
open Utils0
open Word0
open Wsize
open Xseq

let __ = let rec f _ = Obj.repr f in Obj.repr f

type global = { size_of_global : wsize; ident_of_global : Equality.sort }

(** val size_of_global : global -> wsize **)

let size_of_global x = x.size_of_global

(** val global_beq : global -> global -> bool **)

let global_beq g1 g2 =
  let { size_of_global = s1; ident_of_global = n1 } = g1 in
  let { size_of_global = s2; ident_of_global = n2 } = g2 in
  (&&) (eq_op wsize_eqType (Obj.magic s1) (Obj.magic s2))
    (eq_op Ident.Ident.ident n1 n2)

(** val global_eq_axiom : global Equality.axiom **)

let global_eq_axiom _top_assumption_ =
  let _evar_0_ = fun s1 g1 __top_assumption_ ->
    let _evar_0_ = fun s2 g2 ->
      let _evar_0_ = fun _ -> ReflectT in
      let _evar_0_0 = fun _ -> ReflectF in
      (match andP (eq_op wsize_eqType s1 s2) (eq_op Ident.Ident.ident g1 g2) with
       | ReflectT -> _evar_0_ __
       | ReflectF -> _evar_0_0 __)
    in
    let { size_of_global = x; ident_of_global = x0 } = __top_assumption_ in
    Obj.magic _evar_0_ x x0
  in
  let { size_of_global = x; ident_of_global = x0 } = _top_assumption_ in
  Obj.magic _evar_0_ x x0

(** val global_eqMixin : global Equality.mixin_of **)

let global_eqMixin =
  { Equality.op = global_beq; Equality.mixin_of__1 = global_eq_axiom }

(** val global_eqType : Equality.coq_type **)

let global_eqType =
  Obj.magic global_eqMixin

type glob_decl = global * coq_Z

(** val get_global_Z : glob_decl list -> global -> coq_Z option **)

let get_global_Z gd g =
  assoc global_eqType (Obj.magic gd) (Obj.magic g)

(** val get_global_word :
    glob_decl list -> global -> GRing.ComRing.sort exec **)

let get_global_word gd g =
  match get_global_Z gd g with
  | Some z -> Ok (wrepr g.size_of_global z)
  | None -> type_error
