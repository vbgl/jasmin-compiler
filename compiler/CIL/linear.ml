open BinNums
open BinPos
open Datatypes
open Compiler_util
open Eqtype
open Expr
open Stack_alloc
open Type
open Utils0
open Var0
open Wsize
open X86_instr_decl

type label = positive

type linstr_r =
| Lopn of lval list * sopn * pexpr list
| Lalign
| Llabel of label
| Lgoto of label
| Lcond of pexpr * label

type linstr = { li_ii : instr_info; li_i : linstr_r }

type lcmd = linstr list

type lfundef = { lfd_stk_size : coq_Z; lfd_nstk : Equality.sort;
                 lfd_tyin : stype list; lfd_arg : var_i list;
                 lfd_body : lcmd; lfd_tyout : stype list;
                 lfd_res : var_i list;
                 lfd_extra : (Var.var list * saved_stack) }

(** val lfd_stk_size : lfundef -> coq_Z **)

let lfd_stk_size x = x.lfd_stk_size

(** val lfd_nstk : lfundef -> Equality.sort **)

let lfd_nstk x = x.lfd_nstk

(** val lfd_tyin : lfundef -> stype list **)

let lfd_tyin x = x.lfd_tyin

(** val lfd_arg : lfundef -> var_i list **)

let lfd_arg x = x.lfd_arg

(** val lfd_body : lfundef -> lcmd **)

let lfd_body x = x.lfd_body

(** val lfd_tyout : lfundef -> stype list **)

let lfd_tyout x = x.lfd_tyout

(** val lfd_res : lfundef -> var_i list **)

let lfd_res x = x.lfd_res

(** val lfd_extra : lfundef -> Var.var list * saved_stack **)

let lfd_extra x = x.lfd_extra

(** val signature_of_lfundef : lfundef -> function_signature **)

let signature_of_lfundef lfd =
  (lfd.lfd_tyin, lfd.lfd_tyout)

type lprog = (funname * lfundef) list

(** val linear_c :
    (instr -> label -> lcmd -> (label * lcmd) ciexec) -> instr list -> label
    -> lcmd -> (label * lcmd) ciexec **)

let rec linear_c linear_i0 c lbl lc =
  match c with
  | [] -> ciok (lbl, lc)
  | i :: c0 ->
    Result.bind (fun p -> linear_i0 i (fst p) (snd p))
      (linear_c linear_i0 c0 lbl lc)

(** val next_lbl : positive -> positive **)

let next_lbl lbl =
  Pos.add lbl Coq_xH

(** val snot : pexpr -> pexpr **)

let rec snot e = match e with
| Pbool b -> Pbool (negb b)
| Papp1 (s, e0) -> (match s with
                    | Onot -> e0
                    | _ -> Papp1 (Onot, e))
| Papp2 (s, e1, e2) ->
  (match s with
   | Oand -> Papp2 (Oor, (snot e1), (snot e2))
   | Oor -> Papp2 (Oand, (snot e1), (snot e2))
   | _ -> Papp1 (Onot, e))
| Pif (t, e0, e1, e2) -> Pif (t, e0, (snot e1), (snot e2))
| _ -> Papp1 (Onot, e)

(** val add_align : instr_info -> align -> lcmd -> linstr list **)

let add_align ii a lc =
  match a with
  | Align -> { li_ii = ii; li_i = Lalign } :: lc
  | NoAlign -> lc

(** val align :
    instr_info -> align -> (label * lcmd) ciexec -> (label * lcmd) ciexec **)

let align ii a lc =
  Result.bind (fun p -> Ok ((fst p), (add_align ii a (snd p)))) lc

(** val linear_i : instr -> label -> lcmd -> (label * linstr list) ciexec **)

let rec linear_i i lbl lc =
  let MkI (ii, ir) = i in
  (match ir with
   | Cassgn (x, _, ty, e) ->
     (match ty with
      | Coq_sword sz ->
        let op = if cmp_le wsize_cmp sz U64 then MOV sz else VMOVDQU sz in
        Ok (lbl, ({ li_ii = ii; li_i = (Lopn ((x :: []), (Ox86 op),
        (e :: []))) } :: lc))
      | _ ->
        cierror ii (Cerr_linear
          ('a'::('s'::('s'::('i'::('g'::('n'::(' '::('n'::('o'::('t'::(' '::('a'::(' '::('w'::('o'::('r'::('d'::[])))))))))))))))))))
   | Copn (xs, _, o, es) ->
     Ok (lbl, ({ li_ii = ii; li_i = (Lopn (xs, o, es)) } :: lc))
   | Cif (e, c1, c2) ->
     (match c1 with
      | [] ->
        let lbl0 = next_lbl lbl in
        Result.bind (fun p -> Ok ((fst p), ({ li_ii = ii; li_i = (Lcond (e,
          lbl)) } :: (snd p))))
          (linear_c linear_i c2 lbl0 ({ li_ii = ii; li_i = (Llabel
            lbl) } :: lc))
      | _ :: _ ->
        (match c2 with
         | [] ->
           let lbl0 = next_lbl lbl in
           Result.bind (fun p -> Ok ((fst p), ({ li_ii = ii; li_i = (Lcond
             ((snot e), lbl)) } :: (snd p))))
             (linear_c linear_i c1 lbl0 ({ li_ii = ii; li_i = (Llabel
               lbl) } :: lc))
         | _ :: _ ->
           let l2 = next_lbl lbl in
           let lbl0 = next_lbl l2 in
           Result.bind (fun p -> Ok ((fst p), ({ li_ii = ii; li_i = (Lcond
             (e, lbl)) } :: (snd p))))
             (Result.bind (fun p -> linear_c linear_i c2 (fst p) (snd p))
               (Result.bind (fun p -> Ok ((fst p), ({ li_ii = ii; li_i =
                 (Lgoto l2) } :: (snd p))))
                 (Result.bind (fun p -> Ok ((fst p), ({ li_ii = ii; li_i =
                   (Llabel lbl) } :: (snd p))))
                   (linear_c linear_i c1 lbl0 ({ li_ii = ii; li_i = (Llabel
                     l2) } :: lc)))))))
   | Cfor (_, _, _) ->
     cierror ii (Cerr_linear
       ('f'::('o'::('r'::(' '::('f'::('o'::('u'::('n'::('d'::(' '::('i'::('n'::(' '::('l'::('i'::('n'::('e'::('a'::('r'::[]))))))))))))))))))))
   | Cwhile (a, c, e, c') ->
     (match is_bool e with
      | Some b ->
        if b
        then let lbl0 = next_lbl lbl in
             align ii a
               (Result.bind (fun p -> Ok ((fst p), ({ li_ii = ii; li_i =
                 (Llabel lbl) } :: (snd p))))
                 (Result.bind (fun p -> linear_c linear_i c (fst p) (snd p))
                   (linear_c linear_i c' lbl0 ({ li_ii = ii; li_i = (Lgoto
                     lbl) } :: lc))))
        else linear_c linear_i c lbl lc
      | None ->
        (match c' with
         | [] ->
           let lbl0 = next_lbl lbl in
           align ii a
             (Result.bind (fun p -> Ok ((fst p), ({ li_ii = ii; li_i =
               (Llabel lbl) } :: (snd p))))
               (linear_c linear_i c lbl0 ({ li_ii = ii; li_i = (Lcond (e,
                 lbl)) } :: lc)))
         | _ :: _ ->
           let l2 = next_lbl lbl in
           let lbl0 = next_lbl l2 in
           Result.bind (fun p -> Ok ((fst p), ({ li_ii = ii; li_i = (Lgoto
             lbl) } :: (snd p))))
             (align ii a
               (Result.bind (fun p -> Ok ((fst p), ({ li_ii = ii; li_i =
                 (Llabel l2) } :: (snd p))))
                 (Result.bind (fun p -> linear_c linear_i c' (fst p) (snd p))
                   (Result.bind (fun p -> Ok ((fst p), ({ li_ii = ii; li_i =
                     (Llabel lbl) } :: (snd p))))
                     (linear_c linear_i c lbl0 ({ li_ii = ii; li_i = (Lcond
                       (e, l2)) } :: lc))))))))
   | Ccall (_, _, _, _) ->
     cierror ii (Cerr_linear
       ('c'::('a'::('l'::('l'::(' '::('f'::('o'::('u'::('n'::('d'::(' '::('i'::('n'::(' '::('l'::('i'::('n'::('e'::('a'::('r'::[]))))))))))))))))))))))

(** val linear_fd : sfundef -> (instr_info * error_msg, lfundef) result **)

let linear_fd fd =
  Result.bind (fun fd' -> Ok { lfd_stk_size = fd.sf_stk_sz; lfd_nstk =
    fd.sf_stk_id; lfd_tyin = fd.sf_tyin; lfd_arg = fd.sf_params; lfd_body =
    (snd fd'); lfd_tyout = fd.sf_tyout; lfd_res = fd.sf_res; lfd_extra =
    fd.sf_extra }) (linear_c linear_i fd.sf_body Coq_xH [])

(** val linear_prog : sprog -> lprog cfexec **)

let linear_prog p =
  map_cfprog linear_fd p
