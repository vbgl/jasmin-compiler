open BinInt
open BinNums
open Datatypes
open Compiler_util
open Eqtype
open Expr
open Low_memory
open Memory_model
open Seq
open Type
open Utils0
open Var0
open Word0
open Wsize

type saved_stack =
| SavedStackNone
| SavedStackReg of Var.var
| SavedStackStk of coq_Z

type sfundef = { sf_iinfo : instr_info; sf_stk_sz : coq_Z;
                 sf_stk_id : Equality.sort; sf_tyin : stype list;
                 sf_params : var_i list; sf_body : instr list;
                 sf_tyout : stype list; sf_res : var_i list;
                 sf_extra : (Var.var list * saved_stack) }

(** val sf_stk_sz : sfundef -> coq_Z **)

let sf_stk_sz x = x.sf_stk_sz

(** val sf_stk_id : sfundef -> Equality.sort **)

let sf_stk_id x = x.sf_stk_id

(** val sf_tyin : sfundef -> stype list **)

let sf_tyin x = x.sf_tyin

(** val sf_params : sfundef -> var_i list **)

let sf_params x = x.sf_params

(** val sf_body : sfundef -> instr list **)

let sf_body x = x.sf_body

(** val sf_tyout : sfundef -> stype list **)

let sf_tyout x = x.sf_tyout

(** val sf_res : sfundef -> var_i list **)

let sf_res x = x.sf_res

(** val sf_extra : sfundef -> Var.var list * saved_stack **)

let sf_extra x = x.sf_extra

type sprog = (funname * sfundef) list

type map = coq_Z Mvar.t * Equality.sort

(** val size_of : stype -> coq_Z cexec **)

let size_of = function
| Coq_sarr n -> Ok (Zpos n)
| Coq_sword sz -> Ok (wsize_size sz)
| _ ->
  cerror (Cerr_stk_alloc ('s'::('i'::('z'::('e'::('_'::('o'::('f'::[]))))))))

(** val aligned_for : stype -> coq_Z -> bool **)

let aligned_for ty ofs =
  match ty with
  | Coq_sarr _ -> true
  | Coq_sword sz -> is_align Memory.coq_A (wrepr U64 ofs) sz
  | _ -> false

(** val init_map :
    coq_Z -> Equality.sort -> (Var.var * coq_Z) list -> (error_msg, coq_Z
    Mvar.t * Equality.sort) result **)

let init_map sz nstk l =
  let add0 = fun vp mp ->
    let (v, p) = vp in
    if Z.leb (snd mp) p
    then let ty = v.Var.vtype in
         if aligned_for ty (snd vp)
         then Result.bind (fun s ->
                cok ((Mvar.set (fst mp) (Obj.magic v) p), (Z.add p s)))
                (size_of ty)
         else cerror (Cerr_stk_alloc
                ('n'::('o'::('t'::(' '::('a'::('l'::('i'::('g'::('n'::('e'::('d'::[]))))))))))))
    else cerror (Cerr_stk_alloc
           ('o'::('v'::('e'::('r'::('l'::('a'::('p'::[]))))))))
  in
  Result.bind (fun mp ->
    if Z.leb (snd mp) sz
    then cok ((fst mp), nstk)
    else cerror (Cerr_stk_alloc
           ('s'::('t'::('a'::('c'::('k'::(' '::('s'::('i'::('z'::('e'::[]))))))))))))
    (foldM add0 (Mvar.empty, Z0) l)

(** val is_in_stk : map -> Var.var -> bool **)

let is_in_stk m x =
  match Mvar.get (fst m) (Obj.magic x) with
  | Some _ -> true
  | None -> false

(** val vstk : map -> Var.var **)

let vstk m =
  { Var.vtype = (Coq_sword U64); Var.vname = (snd m) }

(** val is_vstk : map -> Var.var -> bool **)

let is_vstk m x =
  eq_op Var.var_eqType (Obj.magic x) (Obj.magic vstk m)

(** val check_var : map -> var_i -> bool **)

let check_var m x =
  (&&) (negb (is_in_stk m x.v_var)) (negb (is_vstk m x.v_var))

(** val cast_w : wsize -> pexpr -> pexpr **)

let cast_w ws x =
  Papp1 ((Oword_of_int ws), x)

(** val cast_ptr : pexpr -> pexpr **)

let cast_ptr =
  cast_w U64

(** val cast_const : coq_Z -> pexpr **)

let cast_const z =
  cast_ptr (Pconst z)

(** val mul : pexpr -> pexpr -> pexpr **)

let mul x x0 =
  Papp2 ((Omul (Op_w U64)), x, x0)

(** val add : pexpr -> pexpr -> pexpr **)

let add x x0 =
  Papp2 ((Oadd (Op_w U64)), x, x0)

(** val cast_word : pexpr -> pexpr **)

let cast_word e = match e with
| Papp1 (s, e1) ->
  (match s with
   | Oint_of_word w -> (match w with
                        | U64 -> e1
                        | _ -> cast_ptr e)
   | _ -> cast_ptr e)
| _ -> cast_ptr e

(** val stk_not_fresh : 'a1 cexec **)

let stk_not_fresh =
  cerror (Cerr_stk_alloc
    ('t'::('h'::('e'::(' '::('s'::('t'::('a'::('c'::('k'::(' '::('v'::('a'::('r'::('i'::('a'::('b'::('l'::('e'::(' '::('i'::('s'::(' '::('n'::('o'::('t'::(' '::('f'::('r'::('e'::('s'::('h'::[]))))))))))))))))))))))))))))))))

(** val not_a_word_v : 'a1 cexec **)

let not_a_word_v =
  cerror (Cerr_stk_alloc
    ('n'::('o'::('t'::(' '::('a'::(' '::('w'::('o'::('r'::('d'::(' '::('v'::('a'::('r'::('i'::('a'::('b'::('l'::('e'::[]))))))))))))))))))))

(** val not_aligned : 'a1 cexec **)

let not_aligned =
  cerror (Cerr_stk_alloc
    ('a'::('r'::('r'::('a'::('y'::(' '::('v'::('a'::('r'::('i'::('a'::('b'::('l'::('e'::(' '::('n'::('o'::('t'::(' '::('a'::('l'::('i'::('g'::('n'::('e'::('d'::[])))))))))))))))))))))))))))

(** val invalid_var : 'a1 cexec **)

let invalid_var =
  cerror (Cerr_stk_alloc
    ('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('v'::('a'::('r'::('i'::('a'::('b'::('l'::('e'::[])))))))))))))))))

(** val mk_ofs : wsize -> pexpr -> coq_Z -> pexpr **)

let mk_ofs ws e1 ofs =
  let sz = wsize_size ws in
  (match is_const e1 with
   | Some i -> cast_const (Z.add (Z.mul i sz) ofs)
   | None -> add (mul (cast_const sz) (cast_word e1)) (cast_const ofs))

(** val alloc_e : map -> pexpr -> (error_msg, pexpr) result **)

let rec alloc_e m e = match e with
| Pvar x ->
  (match Mvar.get (fst m) (Obj.magic v_var x) with
   | Some ofs ->
     (match is_word_type x.v_var.Var.vtype with
      | Some ws ->
        let ofs0 = cast_const ofs in
        let stk = { v_var = (vstk m); v_info = x.v_info } in
        Ok (Pload (ws, stk, ofs0))
      | None -> not_a_word_v)
   | None -> if is_vstk m x.v_var then stk_not_fresh else Ok e)
| Pget (ws, x, e1) ->
  Result.bind (fun e2 ->
    match Mvar.get (fst m) (Obj.magic v_var x) with
    | Some ofs ->
      if is_align Memory.coq_A (wrepr U64 ofs) ws
      then let stk = { v_var = (vstk m); v_info = x.v_info } in
           let ofs0 = mk_ofs ws e2 ofs in Ok (Pload (ws, stk, ofs0))
      else not_aligned
    | None ->
      if is_vstk m x.v_var then stk_not_fresh else Ok (Pget (ws, x, e2)))
    (alloc_e m e1)
| Pload (ws, x, e1) ->
  if check_var m x
  then Result.bind (fun e2 -> Ok (Pload (ws, x, e2))) (alloc_e m e1)
  else invalid_var
| Papp1 (o, e1) -> Result.bind (fun e2 -> Ok (Papp1 (o, e2))) (alloc_e m e1)
| Papp2 (o, e1, e2) ->
  Result.bind (fun e3 ->
    Result.bind (fun e4 -> Ok (Papp2 (o, e3, e4))) (alloc_e m e2))
    (alloc_e m e1)
| PappN (o, es) ->
  Result.bind (fun es0 -> Ok (PappN (o, es0))) (mapM (alloc_e m) es)
| Pif (t0, e0, e1, e2) ->
  Result.bind (fun e3 ->
    Result.bind (fun e4 ->
      Result.bind (fun e5 -> Ok (Pif (t0, e3, e4, e5))) (alloc_e m e2))
      (alloc_e m e1)) (alloc_e m e0)
| _ -> Ok e

(** val alloc_lval :
    map -> lval -> Equality.sort -> (error_msg, lval) result **)

let alloc_lval m r ty =
  match r with
  | Lnone (_, _) -> Ok r
  | Lvar x ->
    (match Mvar.get (fst m) (Obj.magic v_var x) with
     | Some ofs ->
       (match is_word_type x.v_var.Var.vtype with
        | Some ws ->
          if eq_op stype_eqType ty (Obj.magic (Coq_sword ws))
          then let ofs0 = cast_const ofs in
               let stk = { v_var = (vstk m); v_info = x.v_info } in
               Ok (Lmem (ws, stk, ofs0))
          else cerror (Cerr_stk_alloc
                 ('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('t'::('y'::('p'::('e'::(' '::('f'::('o'::('r'::(' '::('L'::('v'::('a'::('r'::[]))))))))))))))))))))))
        | None -> not_a_word_v)
     | None -> if is_vstk m x.v_var then stk_not_fresh else Ok r)
  | Lmem (ws, x, e1) ->
    if check_var m x
    then Result.bind (fun e2 -> Ok (Lmem (ws, x, e2))) (alloc_e m e1)
    else invalid_var
  | Laset (ws, x, e1) ->
    Result.bind (fun e2 ->
      match Mvar.get (fst m) (Obj.magic v_var x) with
      | Some ofs ->
        if is_align Memory.coq_A (wrepr U64 ofs) ws
        then let stk = { v_var = (vstk m); v_info = x.v_info } in
             let ofs0 = mk_ofs ws e2 ofs in Ok (Lmem (ws, stk, ofs0))
        else not_aligned
      | None ->
        if is_vstk m x.v_var then stk_not_fresh else Ok (Laset (ws, x, e2)))
      (alloc_e m e1)

(** val bad_lval_number : error_msg **)

let bad_lval_number =
  Cerr_stk_alloc
    ('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('n'::('u'::('m'::('b'::('e'::('r'::(' '::('o'::('f'::(' '::('l'::('v'::('a'::('l'::[]))))))))))))))))))))))

(** val alloc_i : map -> instr -> (instr_info * error_msg, instr) result **)

let rec alloc_i m = function
| MkI (ii, ir) ->
  Result.bind (fun ir0 -> Ok (MkI (ii, ir0)))
    (match ir with
     | Cassgn (r, t0, ty, e) ->
       Result.bind (fun r0 ->
         Result.bind (fun e0 -> Ok (Cassgn (r0, t0, ty, e0)))
           (add_iinfo ii (alloc_e m e)))
         (add_iinfo ii (alloc_lval m r (Obj.magic ty)))
     | Copn (rs, t0, o, e) ->
       Result.bind (fun rs0 ->
         Result.bind (fun e0 -> Ok (Copn (rs0, t0, o, e0)))
           (add_iinfo ii (mapM (alloc_e m) e)))
         (add_iinfo ii
           (mapM2 bad_lval_number (Obj.magic alloc_lval m) rs (sopn_tout o)))
     | Cif (e, c1, c2) ->
       Result.bind (fun e0 ->
         Result.bind (fun c3 ->
           Result.bind (fun c4 -> Ok (Cif (e0, c3, c4))) (mapM (alloc_i m) c2))
           (mapM (alloc_i m) c1)) (add_iinfo ii (alloc_e m e))
     | Cfor (_, _, _) ->
       cierror ii (Cerr_stk_alloc
         ('d'::('o'::('n'::('\''::('t'::(' '::('d'::('e'::('a'::('l'::(' '::('w'::('i'::('t'::('h'::(' '::('f'::('o'::('r'::(' '::('l'::('o'::('o'::('p'::[])))))))))))))))))))))))))
     | Cwhile (a, c1, e, c2) ->
       Result.bind (fun e0 ->
         Result.bind (fun c3 ->
           Result.bind (fun c4 -> Ok (Cwhile (a, c3, e0, c4)))
             (mapM (alloc_i m) c2)) (mapM (alloc_i m) c1))
         (add_iinfo ii (alloc_e m e))
     | Ccall (_, _, _, _) ->
       cierror ii (Cerr_stk_alloc
         ('d'::('o'::('n'::('\''::('t'::(' '::('d'::('e'::('a'::('l'::(' '::('w'::('i'::('t'::('h'::(' '::('c'::('a'::('l'::('l'::[]))))))))))))))))))))))

(** val add_err_fun : funname -> 'a1 cexec -> (fun_error, 'a1) result **)

let add_err_fun f = function
| Ok a -> Ok a
| Error e -> Error (Ferr_fun (f, e))

(** val alloc_fd :
    (fun_decl -> ((coq_Z * Equality.sort) * (Var.var * coq_Z)
    list) * (Var.var list * saved_stack)) -> fun_decl -> (fun_error,
    funname * sfundef) result **)

let alloc_fd stk_alloc_fd f =
  let info = stk_alloc_fd f in
  let (fn, fd) = f in
  Result.bind (fun sfd -> Ok (fn, sfd))
    (let (p, saved) = info in
     let (p0, l) = p in
     let (size, stkid) = p0 in
     Result.bind (fun m ->
       Result.bind (fun body ->
         if (&&) (all (check_var m) fd.f_params) (all (check_var m) fd.f_res)
         then Ok { sf_iinfo = fd.f_iinfo; sf_stk_sz = size; sf_stk_id =
                stkid; sf_tyin = fd.f_tyin; sf_params = fd.f_params;
                sf_body = body; sf_tyout = fd.f_tyout; sf_res = fd.f_res;
                sf_extra = saved }
         else add_err_fun fn invalid_var)
         (add_finfo fn fn (mapM (alloc_i m) fd.f_body)))
       (add_err_fun fn (init_map size stkid l)))

(** val alloc_prog :
    (fun_decl -> ((coq_Z * Equality.sort) * (Var.var * coq_Z)
    list) * (Var.var list * saved_stack)) -> prog -> (fun_error,
    (funname * sfundef) list) result **)

let alloc_prog stk_alloc_fd p =
  mapM (alloc_fd stk_alloc_fd) p.p_funcs
