open BinNums
open Bool
open Eqtype
open Ssralg
open Ssrbool
open Utils0
open Word0
open Wsize
open Xseq

type global = { size_of_global : wsize; ident_of_global : Equality.sort }

val size_of_global : global -> wsize

val global_beq : global -> global -> bool

val global_eq_axiom : global Equality.axiom

val global_eqMixin : global Equality.mixin_of

val global_eqType : Equality.coq_type

type glob_decl = global * coq_Z

val get_global_Z : glob_decl list -> global -> coq_Z option

val get_global_word : glob_decl list -> global -> GRing.ComRing.sort exec
