open BinNums
open String0
open Compiler_util
open Eqtype
open Expr
open Ssralg
open Ssrfun
open Strings
open Type
open Utils0
open Var0
open Word0
open Wsize
open X86_decl
open Xseq

(** val string_of_register : register -> char list **)

let string_of_register = function
| RAX -> 'R'::('A'::('X'::[]))
| RCX -> 'R'::('C'::('X'::[]))
| RDX -> 'R'::('D'::('X'::[]))
| RBX -> 'R'::('B'::('X'::[]))
| RSP -> 'R'::('S'::('P'::[]))
| RBP -> 'R'::('B'::('P'::[]))
| RSI -> 'R'::('S'::('I'::[]))
| RDI -> 'R'::('D'::('I'::[]))
| R8 -> 'R'::('8'::[])
| R9 -> 'R'::('9'::[])
| R10 -> 'R'::('1'::('0'::[]))
| R11 -> 'R'::('1'::('1'::[]))
| R12 -> 'R'::('1'::('2'::[]))
| R13 -> 'R'::('1'::('3'::[]))
| R14 -> 'R'::('1'::('4'::[]))
| R15 -> 'R'::('1'::('5'::[]))

(** val string_of_rflag : rflag -> char list **)

let string_of_rflag = function
| CF -> 'C'::('F'::[])
| PF -> 'P'::('F'::[])
| ZF -> 'Z'::('F'::[])
| SF -> 'S'::('F'::[])
| OF -> 'O'::('F'::[])
| DF -> 'D'::('F'::[])

(** val regs_strings : (char list * register) list **)

let regs_strings =
  (('R'::('A'::('X'::[]))), RAX) :: ((('R'::('C'::('X'::[]))),
    RCX) :: ((('R'::('D'::('X'::[]))), RDX) :: ((('R'::('B'::('X'::[]))),
    RBX) :: ((('R'::('S'::('P'::[]))), RSP) :: ((('R'::('B'::('P'::[]))),
    RBP) :: ((('R'::('S'::('I'::[]))), RSI) :: ((('R'::('D'::('I'::[]))),
    RDI) :: ((('R'::('8'::[])), R8) :: ((('R'::('9'::[])),
    R9) :: ((('R'::('1'::('0'::[]))), R10) :: ((('R'::('1'::('1'::[]))),
    R11) :: ((('R'::('1'::('2'::[]))), R12) :: ((('R'::('1'::('3'::[]))),
    R13) :: ((('R'::('1'::('4'::[]))), R14) :: ((('R'::('1'::('5'::[]))),
    R15) :: [])))))))))))))))

(** val xmm_regs_strings : (char list * xmm_register) list **)

let xmm_regs_strings =
  (('X'::('M'::('M'::('0'::[])))), XMM0) :: ((('X'::('M'::('M'::('1'::[])))),
    XMM1) :: ((('X'::('M'::('M'::('2'::[])))),
    XMM2) :: ((('X'::('M'::('M'::('3'::[])))),
    XMM3) :: ((('X'::('M'::('M'::('4'::[])))),
    XMM4) :: ((('X'::('M'::('M'::('5'::[])))),
    XMM5) :: ((('X'::('M'::('M'::('6'::[])))),
    XMM6) :: ((('X'::('M'::('M'::('7'::[])))),
    XMM7) :: ((('X'::('M'::('M'::('8'::[])))),
    XMM8) :: ((('X'::('M'::('M'::('9'::[])))),
    XMM9) :: ((('X'::('M'::('M'::('1'::('0'::[]))))),
    XMM10) :: ((('X'::('M'::('M'::('1'::('1'::[]))))),
    XMM11) :: ((('X'::('M'::('M'::('1'::('2'::[]))))),
    XMM12) :: ((('X'::('M'::('M'::('1'::('3'::[]))))),
    XMM13) :: ((('X'::('M'::('M'::('1'::('4'::[]))))),
    XMM14) :: ((('X'::('M'::('M'::('1'::('5'::[]))))),
    XMM15) :: [])))))))))))))))

(** val rflags_strings : (char list * rflag) list **)

let rflags_strings =
  (('C'::('F'::[])), CF) :: ((('P'::('F'::[])), PF) :: ((('Z'::('F'::[])),
    ZF) :: ((('S'::('F'::[])), SF) :: ((('O'::('F'::[])),
    OF) :: ((('D'::('F'::[])), DF) :: [])))))

(** val reg_of_string : char list -> register option **)

let reg_of_string s =
  assoc string_eqType (Obj.magic regs_strings) (Obj.magic s)

(** val xmm_reg_of_string : char list -> xmm_register option **)

let xmm_reg_of_string s =
  assoc string_eqType (Obj.magic xmm_regs_strings) (Obj.magic s)

(** val rflag_of_string : char list -> rflag option **)

let rflag_of_string s =
  assoc string_eqType (Obj.magic rflags_strings) (Obj.magic s)

(** val var_of_register : register -> Var.var **)

let var_of_register r =
  { Var.vtype = (Coq_sword U64); Var.vname =
    (Obj.magic string_of_register r) }

(** val var_of_flag : rflag -> Var.var **)

let var_of_flag f =
  { Var.vtype = Coq_sbool; Var.vname = (Obj.magic string_of_rflag f) }

(** val register_of_var : Var.var -> register option **)

let register_of_var v =
  if eq_op stype_eqType (Obj.magic Var.vtype v) (Obj.magic (Coq_sword U64))
  then reg_of_string (Obj.magic Var.vname v)
  else None

(** val xmm_register_of_var : Var.var -> xmm_register option **)

let xmm_register_of_var v =
  if eq_op stype_eqType (Obj.magic Var.vtype v) (Obj.magic (Coq_sword U256))
  then xmm_reg_of_string (Obj.magic Var.vname v)
  else None

(** val invalid_rflag : char list -> asm_error **)

let invalid_rflag s =
  AsmErr_string
    (append
      ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('r'::('f'::('l'::('a'::('g'::(' '::('n'::('a'::('m'::('e'::(':'::(' '::[]))))))))))))))))))))
      s)

(** val invalid_register : char list -> asm_error **)

let invalid_register s =
  AsmErr_string
    (append
      ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('r'::('e'::('g'::('i'::('s'::('t'::('e'::('r'::(' '::('n'::('a'::('m'::('e'::(':'::(' '::[])))))))))))))))))))))))
      s)

(** val rflag_of_var : instr_info -> Var.var -> rflag ciexec **)

let rflag_of_var ii v =
  let { Var.vtype = vtype0; Var.vname = s } = v in
  (match vtype0 with
   | Coq_sbool ->
     (match rflag_of_string (Obj.magic s) with
      | Some r -> ciok r
      | None -> cierror ii (Cerr_assembler (invalid_rflag (Obj.magic s))))
   | _ ->
     cierror ii (Cerr_assembler (AsmErr_string
       ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('r'::('f'::('l'::('a'::('g'::(' '::('t'::('y'::('p'::('e'::[])))))))))))))))))))))

(** val assemble_cond : instr_info -> pexpr -> condt ciexec **)

let assemble_cond ii e = match e with
| Pvar v ->
  Result.bind (fun r ->
    match r with
    | CF -> Ok B_ct
    | PF -> Ok P_ct
    | ZF -> Ok E_ct
    | SF -> Ok S_ct
    | OF -> Ok O_ct
    | DF ->
      cierror ii (Cerr_assembler (AsmErr_string
        ('C'::('a'::('n'::('n'::('o'::('t'::(' '::('b'::('r'::('a'::('n'::('c'::('h'::(' '::('o'::('n'::(' '::('D'::('F'::[]))))))))))))))))))))))
    (rflag_of_var ii v.v_var)
| Papp1 (s, p) ->
  (match s with
   | Onot ->
     (match p with
      | Pvar v ->
        Result.bind (fun r ->
          match r with
          | CF -> Ok NB_ct
          | PF -> Ok NP_ct
          | ZF -> Ok NE_ct
          | SF -> Ok NS_ct
          | OF -> Ok NO_ct
          | DF ->
            cierror ii (Cerr_assembler (AsmErr_string
              ('C'::('a'::('n'::('n'::('o'::('t'::(' '::('b'::('r'::('a'::('n'::('c'::('h'::(' '::('o'::('n'::(' '::('~'::('~'::(' '::('D'::('F'::[])))))))))))))))))))))))))
          (rflag_of_var ii v.v_var)
      | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
   | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
| Papp2 (s, p, p0) ->
  (match s with
   | Oand ->
     (match p with
      | Papp1 (s0, p1) ->
        (match s0 with
         | Onot ->
           (match p1 with
            | Pvar vzf ->
              (match p0 with
               | Papp1 (s1, p2) ->
                 (match s1 with
                  | Onot ->
                    (match p2 with
                     | Pvar vzf0 ->
                       Result.bind (fun rcf ->
                         Result.bind (fun rzf ->
                           if (&&)
                                (eq_op rflag_eqType (Obj.magic rcf)
                                  (Obj.magic CF))
                                (eq_op rflag_eqType (Obj.magic rzf)
                                  (Obj.magic ZF))
                           then Ok NBE_ct
                           else cierror ii (Cerr_assembler (AsmErr_string
                                  ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::(' '::('('::('N'::('B'::('E'::(')'::[]))))))))))))))))))))))))))
                           (rflag_of_var ii vzf0.v_var))
                         (rflag_of_var ii vzf.v_var)
                     | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
                  | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
               | Pif (_, p2, p3, p4) ->
                 (match p2 with
                  | Pvar vsf ->
                    (match p3 with
                     | Pvar vof1 ->
                       (match p4 with
                        | Papp1 (s1, p5) ->
                          (match s1 with
                           | Onot ->
                             (match p5 with
                              | Pvar vof2 ->
                                Result.bind (fun rzf ->
                                  Result.bind (fun rsf ->
                                    Result.bind (fun rof1 ->
                                      Result.bind (fun rof2 ->
                                        if (&&)
                                             ((&&)
                                               ((&&)
                                                 (eq_op rflag_eqType
                                                   (Obj.magic rzf)
                                                   (Obj.magic ZF))
                                                 (eq_op rflag_eqType
                                                   (Obj.magic rsf)
                                                   (Obj.magic SF)))
                                               (eq_op rflag_eqType
                                                 (Obj.magic rof1)
                                                 (Obj.magic OF)))
                                             (eq_op rflag_eqType
                                               (Obj.magic rof2)
                                               (Obj.magic OF))
                                        then Ok NLE_ct
                                        else cierror ii (Cerr_assembler
                                               (AsmErr_string
                                               ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::(' '::('('::('N'::('L'::('E'::(')'::[]))))))))))))))))))))))))))
                                        (rflag_of_var ii vof2.v_var))
                                      (rflag_of_var ii vof1.v_var))
                                    (rflag_of_var ii vsf.v_var))
                                  (rflag_of_var ii vzf.v_var)
                              | _ ->
                                cierror ii (Cerr_assembler (AsmErr_cond e)))
                           | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
                        | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
                     | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
                  | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
               | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
            | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
         | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
      | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
   | Oor ->
     (match p with
      | Pvar vzf ->
        (match p0 with
         | Pvar vzf0 ->
           Result.bind (fun rcf ->
             Result.bind (fun rzf ->
               if (&&) (eq_op rflag_eqType (Obj.magic rcf) (Obj.magic CF))
                    (eq_op rflag_eqType (Obj.magic rzf) (Obj.magic ZF))
               then Ok BE_ct
               else cierror ii (Cerr_assembler (AsmErr_string
                      ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::(' '::('('::('B'::('E'::(')'::[])))))))))))))))))))))))))
               (rflag_of_var ii vzf0.v_var)) (rflag_of_var ii vzf.v_var)
         | Pif (_, p1, p2, p3) ->
           (match p1 with
            | Pvar vsf ->
              (match p2 with
               | Papp1 (s0, p4) ->
                 (match s0 with
                  | Onot ->
                    (match p4 with
                     | Pvar vof1 ->
                       (match p3 with
                        | Pvar vof2 ->
                          Result.bind (fun rzf ->
                            Result.bind (fun rsf ->
                              Result.bind (fun rof1 ->
                                Result.bind (fun rof2 ->
                                  if (&&)
                                       ((&&)
                                         ((&&)
                                           (eq_op rflag_eqType
                                             (Obj.magic rzf) (Obj.magic ZF))
                                           (eq_op rflag_eqType
                                             (Obj.magic rsf) (Obj.magic SF)))
                                         (eq_op rflag_eqType (Obj.magic rof1)
                                           (Obj.magic OF)))
                                       (eq_op rflag_eqType (Obj.magic rof2)
                                         (Obj.magic OF))
                                  then Ok LE_ct
                                  else cierror ii (Cerr_assembler
                                         (AsmErr_string
                                         ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::(' '::('('::('L'::('E'::(')'::[])))))))))))))))))))))))))
                                  (rflag_of_var ii vof2.v_var))
                                (rflag_of_var ii vof1.v_var))
                              (rflag_of_var ii vsf.v_var))
                            (rflag_of_var ii vzf.v_var)
                        | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
                     | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
                  | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
               | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
            | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
         | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
      | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
   | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
| Pif (_, p, p0, p1) ->
  (match p with
   | Pvar vsf ->
     (match p0 with
      | Pvar vof1 ->
        (match p1 with
         | Papp1 (s, p2) ->
           (match s with
            | Onot ->
              (match p2 with
               | Pvar vof2 ->
                 Result.bind (fun rsf ->
                   Result.bind (fun rof1 ->
                     Result.bind (fun rof2 ->
                       if (&&)
                            ((&&)
                              (eq_op rflag_eqType (Obj.magic rsf)
                                (Obj.magic SF))
                              (eq_op rflag_eqType (Obj.magic rof1)
                                (Obj.magic OF)))
                            (eq_op rflag_eqType (Obj.magic rof2)
                              (Obj.magic OF))
                       then Ok NL_ct
                       else cierror ii (Cerr_assembler (AsmErr_string
                              ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::(' '::('('::('N'::('L'::(')'::[])))))))))))))))))))))))))
                       (rflag_of_var ii vof2.v_var))
                     (rflag_of_var ii vof1.v_var)) (rflag_of_var ii vsf.v_var)
               | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
            | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
         | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
      | Papp1 (s, p2) ->
        (match s with
         | Onot ->
           (match p2 with
            | Pvar vof1 ->
              (match p1 with
               | Pvar vof2 ->
                 Result.bind (fun rsf ->
                   Result.bind (fun rof1 ->
                     Result.bind (fun rof2 ->
                       if (&&)
                            ((&&)
                              (eq_op rflag_eqType (Obj.magic rsf)
                                (Obj.magic SF))
                              (eq_op rflag_eqType (Obj.magic rof1)
                                (Obj.magic OF)))
                            (eq_op rflag_eqType (Obj.magic rof2)
                              (Obj.magic OF))
                       then Ok L_ct
                       else cierror ii (Cerr_assembler (AsmErr_string
                              ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('c'::('o'::('n'::('d'::('i'::('t'::('i'::('o'::('n'::(' '::('('::('L'::(')'::[]))))))))))))))))))))))))
                       (rflag_of_var ii vof2.v_var))
                     (rflag_of_var ii vof1.v_var)) (rflag_of_var ii vsf.v_var)
               | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
            | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
         | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
      | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
   | _ -> cierror ii (Cerr_assembler (AsmErr_cond e)))
| _ -> cierror ii (Cerr_assembler (AsmErr_cond e))

(** val reg_of_var : instr_info -> Var.var -> register ciexec **)

let reg_of_var ii v =
  let { Var.vtype = vtype0; Var.vname = s } = v in
  (match vtype0 with
   | Coq_sword w ->
     (match w with
      | U64 ->
        (match reg_of_string (Obj.magic s) with
         | Some r -> ciok r
         | None ->
           cierror ii (Cerr_assembler (invalid_register (Obj.magic s))))
      | _ ->
        cierror ii (Cerr_assembler (AsmErr_string
          ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('r'::('e'::('g'::('i'::('s'::('t'::('e'::('r'::(' '::('t'::('y'::('p'::('e'::[]))))))))))))))))))))))))
   | _ ->
     cierror ii (Cerr_assembler (AsmErr_string
       ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('r'::('e'::('g'::('i'::('s'::('t'::('e'::('r'::(' '::('t'::('y'::('p'::('e'::[]))))))))))))))))))))))))

(** val reg_of_vars :
    instr_info -> var_i list -> (instr_info * error_msg, register list) result **)

let reg_of_vars ii vs =
  mapM (funcomp () (reg_of_var ii) v_var) vs

(** val scale_of_z' : instr_info -> GRing.ComRing.sort -> scale ciexec **)

let scale_of_z' ii z =
  match wunsigned U64 z with
  | Zpos p ->
    (match p with
     | Coq_xI _ ->
       cierror ii (Cerr_assembler (AsmErr_string
         ('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('s'::('c'::('a'::('l'::('e'::[])))))))))))))))
     | Coq_xO p0 ->
       (match p0 with
        | Coq_xI _ ->
          cierror ii (Cerr_assembler (AsmErr_string
            ('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('s'::('c'::('a'::('l'::('e'::[])))))))))))))))
        | Coq_xO p1 ->
          (match p1 with
           | Coq_xI _ ->
             cierror ii (Cerr_assembler (AsmErr_string
               ('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('s'::('c'::('a'::('l'::('e'::[])))))))))))))))
           | Coq_xO p2 ->
             (match p2 with
              | Coq_xH -> Ok Scale8
              | _ ->
                cierror ii (Cerr_assembler (AsmErr_string
                  ('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('s'::('c'::('a'::('l'::('e'::[]))))))))))))))))
           | Coq_xH -> Ok Scale4)
        | Coq_xH -> Ok Scale2)
     | Coq_xH -> Ok Scale1)
  | _ ->
    cierror ii (Cerr_assembler (AsmErr_string
      ('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('s'::('c'::('a'::('l'::('e'::[])))))))))))))))

type ofs =
| Ofs_const of GRing.ComRing.sort
| Ofs_var of Var.var
| Ofs_mul of GRing.ComRing.sort * Var.var
| Ofs_add of GRing.ComRing.sort * Var.var * GRing.ComRing.sort
| Ofs_error

(** val addr_ofs : pexpr -> ofs **)

let rec addr_ofs = function
| Pvar x -> Ofs_var x.v_var
| Papp1 (s, p) ->
  (match s with
   | Oword_of_int w ->
     (match w with
      | U64 ->
        (match p with
         | Pconst z -> Ofs_const (wrepr U64 z)
         | _ -> Ofs_error)
      | _ -> Ofs_error)
   | _ -> Ofs_error)
| Papp2 (s, e1, e2) ->
  (match s with
   | Oadd o ->
     (match o with
      | Op_int -> Ofs_error
      | Op_w w ->
        (match w with
         | U64 ->
           (match addr_ofs e1 with
            | Ofs_const n ->
              (match addr_ofs e2 with
               | Ofs_const n2 ->
                 Ofs_const
                   (GRing.add (GRing.ComRing.zmodType (word U64)) n n2)
               | Ofs_var x ->
                 Ofs_add ((GRing.one (GRing.ComRing.ringType (word U64))), x,
                   n)
               | Ofs_mul (sc, x) -> Ofs_add (sc, x, n)
               | _ -> Ofs_error)
            | Ofs_var x ->
              (match addr_ofs e2 with
               | Ofs_const n ->
                 Ofs_add ((GRing.one (GRing.ComRing.ringType (word U64))), x,
                   n)
               | _ -> Ofs_error)
            | Ofs_mul (sc, x) ->
              (match addr_ofs e2 with
               | Ofs_const n -> Ofs_add (sc, x, n)
               | _ -> Ofs_error)
            | _ -> Ofs_error)
         | _ -> Ofs_error))
   | Omul o ->
     (match o with
      | Op_int -> Ofs_error
      | Op_w w ->
        (match w with
         | U64 ->
           (match addr_ofs e1 with
            | Ofs_const sc ->
              (match addr_ofs e2 with
               | Ofs_const n2 ->
                 Ofs_const
                   (GRing.mul (GRing.ComRing.ringType (word U64)) sc n2)
               | Ofs_var x -> Ofs_mul (sc, x)
               | _ -> Ofs_error)
            | Ofs_var x ->
              (match addr_ofs e2 with
               | Ofs_const sc -> Ofs_mul (sc, x)
               | _ -> Ofs_error)
            | _ -> Ofs_error)
         | _ -> Ofs_error))
   | _ -> Ofs_error)
| _ -> Ofs_error

(** val addr_of_pexpr : instr_info -> register -> pexpr -> address ciexec **)

let addr_of_pexpr ii s e =
  match addr_ofs e with
  | Ofs_const z ->
    ciok { ad_disp = z; ad_base = (Some s); ad_scale = Scale1; ad_offset =
      None }
  | Ofs_var x ->
    Result.bind (fun x0 ->
      ciok { ad_disp = (GRing.zero (GRing.ComRing.zmodType (word U64)));
        ad_base = (Some s); ad_scale = Scale1; ad_offset = (Some x0) })
      (reg_of_var ii x)
  | Ofs_mul (sc, x) ->
    Result.bind (fun x0 ->
      Result.bind (fun sc0 ->
        ciok { ad_disp = (GRing.zero (GRing.ComRing.zmodType (word U64)));
          ad_base = (Some s); ad_scale = sc0; ad_offset = (Some x0) })
        (scale_of_z' ii sc)) (reg_of_var ii x)
  | Ofs_add (sc, x, z) ->
    Result.bind (fun x0 ->
      Result.bind (fun sc0 ->
        ciok { ad_disp = z; ad_base = (Some s); ad_scale = sc0; ad_offset =
          (Some x0) }) (scale_of_z' ii sc)) (reg_of_var ii x)
  | Ofs_error ->
    cierror ii (Cerr_assembler (AsmErr_string
      ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('a'::('d'::('d'::('r'::('e'::('s'::('s'::(' '::('e'::('x'::('p'::('r'::('e'::('s'::('s'::('i'::('o'::('n'::[]))))))))))))))))))))))))))))

(** val xreg_of_var : instr_info -> Var.var -> asm_arg ciexec **)

let xreg_of_var ii x =
  match xmm_register_of_var x with
  | Some r -> Ok (XMM r)
  | None ->
    (match register_of_var x with
     | Some r -> Ok (Reg r)
     | None ->
       cierror ii (Cerr_assembler (AsmErr_string
         ('N'::('o'::('t'::(' '::('a'::(' '::('('::('x'::(')'::('r'::('e'::('g'::('i'::('s'::('t'::('e'::('r'::[]))))))))))))))))))))

(** val assemble_word :
    instr_info -> wsize -> wsize option -> pexpr -> asm_arg ciexec **)

let assemble_word ii sz max_imm = function
| Pvar x -> xreg_of_var ii x.v_var
| Pglobal g -> Ok (Glob g)
| Pload (sz', v, e0) ->
  if eq_op wsize_eqType (Obj.magic sz) (Obj.magic sz')
  then Result.bind (fun s ->
         Result.bind (fun w -> Ok (Adr w)) (addr_of_pexpr ii s e0))
         (reg_of_var ii v.v_var)
  else cierror ii (Cerr_assembler (AsmErr_string
         ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('p'::('e'::('x'::('p'::('r'::(' '::('f'::('o'::('r'::(' '::('w'::('o'::('r'::('d'::(':'::(' '::('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('L'::('o'::('a'::('d'::(' '::('s'::('i'::('z'::('e'::[])))))))))))))))))))))))))))))))))))))))))))
| Papp1 (s, p) ->
  (match s with
   | Oword_of_int sz' ->
     (match p with
      | Pconst z ->
        (match max_imm with
         | Some sz1 ->
           let w = wrepr sz1 z in
           let w1 = sign_extend sz sz1 w in
           let w2 = zero_extend sz sz' (wrepr sz' z) in
           Result.bind (fun _ -> ciok (Imm (sz1, w)))
             (coq_assert (eq_op (GRing.ComRing.eqType (word sz)) w1 w2) (ii,
               (Cerr_assembler (AsmErr_string
               ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('p'::('e'::('x'::('p'::('r'::(' '::('f'::('o'::('r'::(' '::('o'::('p'::('r'::('d'::(':'::(' '::('o'::('u'::('t'::(' '::('o'::('f'::(' '::('b'::('o'::('u'::('n'::('d'::(' '::('c'::('o'::('n'::('s'::('t'::('a'::('n'::('t'::[])))))))))))))))))))))))))))))))))))))))))))))))))
         | None ->
           cierror ii (Cerr_assembler (AsmErr_string
             ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('p'::('e'::('x'::('p'::('r'::(' '::('f'::('o'::('r'::(' '::('o'::('p'::('r'::('d'::(','::(' '::('c'::('o'::('n'::('s'::('t'::('a'::('n'::('t'::(' '::('n'::('o'::('t'::(' '::('a'::('l'::('l'::('o'::('w'::('e'::('d'::[])))))))))))))))))))))))))))))))))))))))))))))))
      | _ ->
        cierror ii (Cerr_assembler (AsmErr_string
          ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('p'::('e'::('x'::('p'::('r'::(' '::('f'::('o'::('r'::(' '::('w'::('o'::('r'::('d'::[])))))))))))))))))))))))))
   | _ ->
     cierror ii (Cerr_assembler (AsmErr_string
       ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('p'::('e'::('x'::('p'::('r'::(' '::('f'::('o'::('r'::(' '::('w'::('o'::('r'::('d'::[])))))))))))))))))))))))))
| _ ->
  cierror ii (Cerr_assembler (AsmErr_string
    ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('p'::('e'::('x'::('p'::('r'::(' '::('f'::('o'::('r'::(' '::('w'::('o'::('r'::('d'::[]))))))))))))))))))))))))

(** val arg_of_pexpr :
    instr_info -> stype -> wsize option -> pexpr -> (instr_info * error_msg,
    asm_arg) result **)

let arg_of_pexpr ii ty max_imm e =
  match ty with
  | Coq_sbool -> Result.bind (fun c -> Ok (Condt c)) (assemble_cond ii e)
  | Coq_sint ->
    cierror ii (Cerr_assembler (AsmErr_string
      ('s'::('i'::('n'::('t'::(' '::('?'::('?'::('?'::[]))))))))))
  | Coq_sarr _ ->
    cierror ii (Cerr_assembler (AsmErr_string
      ('s'::('a'::('r'::('r'::(' '::('?'::('?'::('?'::[]))))))))))
  | Coq_sword sz -> assemble_word ii sz max_imm e
