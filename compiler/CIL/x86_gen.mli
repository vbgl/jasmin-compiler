open BinNums
open Datatypes
open String0
open Asmgen
open Compiler_util
open Expr
open Linear
open Seq
open Ssrbool
open Ssrfun
open Stack_alloc
open Utils0
open X86_decl
open X86_sem
open X86_variables

val assemble_i : linstr -> asm ciexec

val assemble_c : lcmd -> asm list ciexec

type x86_gen_error_t =
| X86Error_InvalidStackPointer
| X86Error_StackPointerInArguments of register

val x86_gen_error : x86_gen_error_t -> instr_info * error_msg

val assemble_saved_stack :
  Stack_alloc.saved_stack -> (instr_info * error_msg, saved_stack) result

val assemble_fd : lfundef -> (instr_info * error_msg, xfundef) result

val assemble_prog : lprog -> xprog cfexec
