open BinNums
open BinPos
open Datatypes
open Compiler_util
open Eqtype
open Expr
open Stack_alloc
open Type
open Utils0
open Var0
open Wsize
open X86_instr_decl

type label = positive

type linstr_r =
| Lopn of lval list * sopn * pexpr list
| Lalign
| Llabel of label
| Lgoto of label
| Lcond of pexpr * label

type linstr = { li_ii : instr_info; li_i : linstr_r }

type lcmd = linstr list

type lfundef = { lfd_stk_size : coq_Z; lfd_nstk : Equality.sort;
                 lfd_tyin : stype list; lfd_arg : var_i list;
                 lfd_body : lcmd; lfd_tyout : stype list;
                 lfd_res : var_i list;
                 lfd_extra : (Var.var list * saved_stack) }

val lfd_stk_size : lfundef -> coq_Z

val lfd_nstk : lfundef -> Equality.sort

val lfd_tyin : lfundef -> stype list

val lfd_arg : lfundef -> var_i list

val lfd_body : lfundef -> lcmd

val lfd_tyout : lfundef -> stype list

val lfd_res : lfundef -> var_i list

val lfd_extra : lfundef -> Var.var list * saved_stack

val signature_of_lfundef : lfundef -> function_signature

type lprog = (funname * lfundef) list

val linear_c :
  (instr -> label -> lcmd -> (label * lcmd) ciexec) -> instr list -> label ->
  lcmd -> (label * lcmd) ciexec

val next_lbl : positive -> positive

val snot : pexpr -> pexpr

val add_align : instr_info -> align -> lcmd -> linstr list

val align :
  instr_info -> align -> (label * lcmd) ciexec -> (label * lcmd) ciexec

val linear_i : instr -> label -> lcmd -> (label * linstr list) ciexec

val linear_fd : sfundef -> (instr_info * error_msg, lfundef) result

val linear_prog : sprog -> lprog cfexec
