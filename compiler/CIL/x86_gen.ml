open BinNums
open Datatypes
open String0
open Asmgen
open Compiler_util
open Expr
open Linear
open Seq
open Ssrbool
open Ssrfun
open Stack_alloc
open Utils0
open X86_decl
open X86_sem
open X86_variables

(** val assemble_i : linstr -> asm ciexec **)

let assemble_i i =
  let { li_ii = ii; li_i = ir } = i in
  (match ir with
   | Lopn (ds, op, es) ->
     Result.bind (fun oa -> Ok (AsmOp ((fst oa), (snd oa))))
       (assemble_sopn ii op ds es)
   | Lalign -> ciok ALIGN
   | Llabel lbl -> ciok (LABEL lbl)
   | Lgoto lbl -> ciok (JMP lbl)
   | Lcond (e, l) ->
     Result.bind (fun cond -> ciok (Jcc (l, cond))) (assemble_cond ii e))

(** val assemble_c : lcmd -> asm list ciexec **)

let assemble_c lc =
  mapM assemble_i lc

type x86_gen_error_t =
| X86Error_InvalidStackPointer
| X86Error_StackPointerInArguments of register

(** val x86_gen_error : x86_gen_error_t -> instr_info * error_msg **)

let x86_gen_error e =
  (Coq_xH, (Cerr_assembler (AsmErr_string
    (match e with
     | X86Error_InvalidStackPointer ->
       'I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('s'::('t'::('a'::('c'::('k'::(' '::('p'::('o'::('i'::('n'::('t'::('e'::('r'::[]))))))))))))))))))))
     | X86Error_StackPointerInArguments sp ->
       append
         ('S'::('t'::('a'::('c'::('k'::(' '::('p'::('o'::('i'::('n'::('t'::('e'::('r'::(' '::('('::[])))))))))))))))
         (append (string_of_register sp)
           (')'::(' '::('i'::('s'::(' '::('a'::('l'::('s'::('o'::(' '::('a'::('n'::(' '::('a'::('r'::('g'::('u'::('m'::('e'::('n'::('t'::[]))))))))))))))))))))))))))

(** val assemble_saved_stack :
    Stack_alloc.saved_stack -> (instr_info * error_msg, saved_stack) result **)

let assemble_saved_stack = function
| Stack_alloc.SavedStackNone -> Ok SavedStackNone
| Stack_alloc.SavedStackReg r ->
  Result.bind (fun r0 -> Ok (SavedStackReg r0)) (reg_of_var Coq_xH r)
| Stack_alloc.SavedStackStk z -> Ok (SavedStackStk z)

(** val assemble_fd : lfundef -> (instr_info * error_msg, xfundef) result **)

let assemble_fd fd =
  Result.bind (fun fd' ->
    match reg_of_string (Obj.magic lfd_nstk fd) with
    | Some sp ->
      Result.bind (fun arg ->
        Result.bind (fun res ->
          Result.bind (fun _ ->
            Result.bind (fun tosave ->
              Result.bind (fun saved ->
                ciok { xfd_stk_size = fd.lfd_stk_size; xfd_nstk = sp;
                  xfd_arg = arg; xfd_body = fd'; xfd_res = res; xfd_extra =
                  (tosave, saved) }) (assemble_saved_stack (snd fd.lfd_extra)))
              (reg_of_vars Coq_xH
                (map (fun x -> { v_var = x; v_info = Coq_xH })
                  (fst fd.lfd_extra))))
            (coq_assert
              (negb
                (in_mem (Obj.magic (Reg sp))
                  (mem (seq_predType asm_arg_eqType) (Obj.magic arg))))
              (x86_gen_error (X86Error_StackPointerInArguments sp))))
          (mapM (funcomp () (xreg_of_var Coq_xH) v_var) fd.lfd_res))
        (mapM (funcomp () (xreg_of_var Coq_xH) v_var) fd.lfd_arg)
    | None -> Error (x86_gen_error X86Error_InvalidStackPointer))
    (assemble_c fd.lfd_body)

(** val assemble_prog : lprog -> xprog cfexec **)

let assemble_prog p =
  map_cfprog assemble_fd p
