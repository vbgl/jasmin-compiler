open BinNums
open Eqtype
open Utils0
open X86_decl
open X86_instr_decl

type asm =
| ALIGN
| LABEL of label
| JMP of label
| Jcc of label * condt
| AsmOp of asm_op * asm_arg list

val check_oreg : Equality.sort option -> asm_arg -> bool

type saved_stack =
| SavedStackNone
| SavedStackReg of register
| SavedStackStk of coq_Z

type xfundef = { xfd_stk_size : coq_Z; xfd_nstk : register;
                 xfd_arg : asm_arg list; xfd_body : asm list;
                 xfd_res : asm_arg list;
                 xfd_extra : (register list * saved_stack) }

type xprog = (funname * xfundef) list
