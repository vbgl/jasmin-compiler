open BinNums
open Datatypes
open String0
open Compiler_util
open Eqtype
open Expr
open Lowering
open Oseq
open Sem
open Seq
open Ssralg
open Ssrnat
open Type
open Utils0
open Var0
open Word0
open Wsize
open X86_decl
open X86_instr_decl
open X86_sem
open X86_variables

(** val pexpr_of_lval : instr_info -> lval -> pexpr ciexec **)

let pexpr_of_lval ii = function
| Lvar x -> Ok (Pvar x)
| Lmem (s, x, e) -> Ok (Pload (s, x, e))
| _ ->
  cierror ii (Cerr_assembler (AsmErr_string
    ('p'::('e'::('x'::('p'::('r'::('_'::('o'::('f'::('_'::('l'::('v'::('a'::('l'::[])))))))))))))))

type 't nmap = nat -> 't option

(** val nget : 'a1 nmap -> nat -> 'a1 option **)

let nget m =
  m

(** val nset : 'a1 nmap -> nat -> 'a1 -> Equality.sort -> 'a1 option **)

let nset m n t x =
  if eq_op nat_eqType x (Obj.magic n) then Some t else nget m (Obj.magic x)

(** val nempty : nat -> 'a1 option **)

let nempty _ =
  None

(** val var_of_implicit : implicite_arg -> Var.var **)

let var_of_implicit = function
| IArflag f -> var_of_flag f
| IAreg r -> var_of_register r

(** val compile_arg :
    instr_info -> wsize option -> ((arg_desc * stype) * pexpr) -> asm_arg
    nmap -> asm_arg nmap ciexec **)

let compile_arg ii max_imm ade m =
  let ad = fst ade in
  let e = snd ade in
  (match fst ad with
   | ADImplicit i ->
     Result.bind (fun _ -> Ok m)
       (coq_assert
         (eq_expr (Pvar { v_var = (var_of_implicit i); v_info = Coq_xH }) e)
         (ii, (Cerr_assembler (AsmErr_string
         ('c'::('o'::('m'::('p'::('i'::('l'::('e'::('_'::('a'::('r'::('g'::(' '::(':'::(' '::('b'::('a'::('d'::(' '::('i'::('m'::('p'::('l'::('i'::('c'::('i'::('t'::[]))))))))))))))))))))))))))))))
   | ADExplicit (n, o) ->
     Result.bind (fun a ->
       Result.bind (fun _ ->
         match nget m n with
         | Some a' ->
           if eq_op asm_arg_eqType (Obj.magic a) (Obj.magic a')
           then Ok m
           else cierror ii (Cerr_assembler (AsmErr_string
                  ('c'::('o'::('m'::('p'::('i'::('l'::('e'::('_'::('a'::('r'::('g'::(' '::(':'::(' '::('n'::('o'::('t'::(' '::('c'::('o'::('m'::('p'::('a'::('t'::('i'::('b'::('l'::('e'::(' '::('a'::('s'::('m'::('_'::('a'::('r'::('g'::[]))))))))))))))))))))))))))))))))))))))
         | None -> Ok (Obj.magic nset m n a))
         (coq_assert (check_oreg (Obj.magic o) a) (ii, (Cerr_assembler
           (AsmErr_string
           ('c'::('o'::('m'::('p'::('i'::('l'::('e'::('_'::('a'::('r'::('g'::(' '::(':'::(' '::('b'::('a'::('d'::(' '::('f'::('o'::('r'::('c'::('e'::('d'::(' '::('r'::('e'::('g'::('i'::('s'::('t'::('e'::('r'::[]))))))))))))))))))))))))))))))))))))))
       (arg_of_pexpr ii (snd ad) max_imm e))

(** val compile_args :
    instr_info -> wsize option -> (arg_desc * stype) list -> pexpr list ->
    asm_arg nmap -> (instr_info * error_msg, asm_arg nmap) result **)

let compile_args ii max_imm adts es m =
  foldM (compile_arg ii max_imm) m (zip adts es)

(** val compat_imm : stype -> Equality.sort -> Equality.sort -> bool **)

let compat_imm ty a' a =
  (||) (eq_op asm_arg_eqType a a')
    (match ty with
     | Coq_sword sz ->
       (match Obj.magic a with
        | Imm (sz1, w1) ->
          (match Obj.magic a' with
           | Imm (sz2, w2) ->
             eq_op (GRing.ComRing.eqType (word sz)) (sign_extend sz sz1 w1)
               (sign_extend sz sz2 w2)
           | _ -> false)
        | _ -> false)
     | _ -> false)

(** val check_sopn_arg :
    instr_info -> wsize option -> asm_arg list -> pexpr -> (arg_desc * stype)
    -> bool **)

let check_sopn_arg ii max_imm loargs x adt =
  match fst adt with
  | ADImplicit i ->
    eq_expr x (Pvar { v_var = (var_of_implicit i); v_info = Coq_xH })
  | ADExplicit (n, o) ->
    (match onth loargs n with
     | Some a ->
       (match arg_of_pexpr ii (snd adt) max_imm x with
        | Ok a' ->
          (&&) (compat_imm (snd adt) (Obj.magic a) (Obj.magic a'))
            (check_oreg (Obj.magic o) a)
        | Error _ -> false)
     | None -> false)

(** val check_sopn_dest :
    instr_info -> wsize option -> asm_arg list -> pexpr -> (arg_desc * stype)
    -> bool **)

let check_sopn_dest ii max_imm loargs x adt =
  match fst adt with
  | ADImplicit i ->
    eq_expr x (Pvar { v_var = (var_of_implicit i); v_info = Coq_xH })
  | ADExplicit (n, o) ->
    (match onth loargs n with
     | Some a ->
       (match arg_of_pexpr ii (snd adt) max_imm x with
        | Ok a' ->
          (&&) (eq_op asm_arg_eqType (Obj.magic a) (Obj.magic a'))
            (check_oreg (Obj.magic o) a)
        | Error _ -> false)
     | None -> false)

(** val error_imm : error_msg **)

let error_imm =
  Cerr_assembler (AsmErr_string
    ('I'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('a'::('s'::('m'::(':'::(' '::('c'::('a'::('n'::('n'::('o'::('t'::(' '::('t'::('r'::('u'::('n'::('c'::('a'::('t'::('e'::(' '::('t'::('h'::('e'::(' '::('i'::('m'::('m'::('e'::('d'::('i'::('a'::('t'::('e'::(' '::('t'::('o'::(' '::('a'::(' '::('3'::('2'::(' '::('b'::('i'::('t'::('s'::(' '::('i'::('m'::('m'::('e'::('d'::('i'::('a'::('t'::('e'::(','::(' '::('m'::('o'::('v'::('e'::(' '::('i'::('t'::(' '::('t'::('o'::(' '::('a'::(' '::('r'::('e'::('g'::('i'::('s'::('t'::('e'::('r'::(' '::('f'::('i'::('r'::('s'::('t'::[])))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))

(** val assemble_x86_opn_aux :
    instr_info -> asm_op -> lval list -> pexpr list ->
    (instr_info * error_msg, asm_arg list) result **)

let assemble_x86_opn_aux ii op outx inx =
  let id = instr_desc op in
  let max_imm = id.id_max_imm in
  Result.bind (fun m ->
    Result.bind (fun eoutx ->
      Result.bind (fun m0 ->
        match omap (nget m0) (iota O id.id_nargs) with
        | Some asm_args ->
          Result.bind (fun asm_args0 -> Ok asm_args0)
            (match op with
             | MOV w0 ->
               (match w0 with
                | U64 ->
                  (match asm_args with
                   | [] -> Ok asm_args
                   | y :: l ->
                     (match y with
                      | Adr a ->
                        (match l with
                         | [] -> Ok asm_args
                         | a0 :: l0 ->
                           (match a0 with
                            | Imm (ws, w) ->
                              (match ws with
                               | U64 ->
                                 (match l0 with
                                  | [] ->
                                    (match truncate_word U32 U64 w with
                                     | Ok w' ->
                                       Result.bind (fun _ -> Ok ((Adr
                                         a) :: ((Imm (U32, w')) :: [])))
                                         (coq_assert
                                           (eq_op
                                             (GRing.ComRing.eqType (word U64))
                                             (sign_extend U64 U32 w') w) (ii,
                                           error_imm))
                                     | Error _ -> cierror ii error_imm)
                                  | _ :: _ -> Ok asm_args)
                               | _ -> Ok asm_args)
                            | _ -> Ok asm_args))
                      | _ -> Ok asm_args))
                | _ -> Ok asm_args)
             | _ -> Ok asm_args)
        | None ->
          cierror ii (Cerr_assembler (AsmErr_string
            ('c'::('o'::('m'::('p'::('i'::('l'::('e'::('_'::('a'::('r'::('g'::(' '::(':'::(' '::('a'::('s'::('s'::('e'::('r'::('t'::(' '::('f'::('a'::('l'::('s'::('e'::(' '::('n'::('g'::('e'::('t'::[]))))))))))))))))))))))))))))))))))
        (compile_args ii max_imm (zip id.id_out id.id_tout) eoutx m))
      (mapM (pexpr_of_lval ii) outx))
    (compile_args ii max_imm (zip id.id_in id.id_tin) inx nempty)

(** val check_sopn_args :
    instr_info -> wsize option -> asm_arg list -> pexpr list ->
    (arg_desc * stype) list -> bool **)

let check_sopn_args ii max_imm loargs xs adt =
  Oseq.all2 (check_sopn_arg ii max_imm loargs) xs adt

(** val check_sopn_dests :
    instr_info -> wsize option -> asm_arg list -> lval list ->
    (arg_desc * stype) list -> bool **)

let check_sopn_dests ii max_imm loargs outx adt =
  match mapM (pexpr_of_lval ii) outx with
  | Ok eoutx -> Oseq.all2 (check_sopn_dest ii max_imm loargs) eoutx adt
  | Error _ -> false

(** val is_lea :
    instr_info -> asm_op -> lval list -> pexpr list ->
    (instr_info * error_msg, ((wsize * var_i) * lea) option) result **)

let is_lea ii op outx inx =
  match op with
  | LEA sz ->
    (match outx with
     | [] ->
       cierror ii (Cerr_assembler (AsmErr_string
         ('l'::('e'::('a'::(':'::(' '::('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('l'::('e'::('a'::(' '::('i'::('n'::('s'::('t'::('r'::('u'::('c'::('t'::('i'::('o'::('n'::[]))))))))))))))))))))))))))))))
     | l :: l0 ->
       (match l with
        | Lvar x ->
          (match l0 with
           | [] ->
             (match inx with
              | [] ->
                cierror ii (Cerr_assembler (AsmErr_string
                  ('l'::('e'::('a'::(':'::(' '::('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('l'::('e'::('a'::(' '::('i'::('n'::('s'::('t'::('r'::('u'::('c'::('t'::('i'::('o'::('n'::[]))))))))))))))))))))))))))))))
              | e :: l1 ->
                (match l1 with
                 | [] ->
                   (match mk_lea sz e with
                    | Some lea0 -> Ok (Some ((sz, x), lea0))
                    | None ->
                      cierror ii (Cerr_assembler (AsmErr_string
                        ('l'::('e'::('a'::(':'::(' '::('n'::('o'::('t'::(' '::('a'::('b'::('l'::('e'::(' '::('t'::('o'::(' '::('a'::('s'::('s'::('e'::('m'::('b'::('l'::('e'::(' '::('a'::('d'::('d'::('r'::('e'::('s'::('s'::[]))))))))))))))))))))))))))))))))))))
                 | _ :: _ ->
                   cierror ii (Cerr_assembler (AsmErr_string
                     ('l'::('e'::('a'::(':'::(' '::('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('l'::('e'::('a'::(' '::('i'::('n'::('s'::('t'::('r'::('u'::('c'::('t'::('i'::('o'::('n'::[]))))))))))))))))))))))))))))))))
           | _ :: _ ->
             cierror ii (Cerr_assembler (AsmErr_string
               ('l'::('e'::('a'::(':'::(' '::('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('l'::('e'::('a'::(' '::('i'::('n'::('s'::('t'::('r'::('u'::('c'::('t'::('i'::('o'::('n'::[])))))))))))))))))))))))))))))))
        | _ ->
          cierror ii (Cerr_assembler (AsmErr_string
            ('l'::('e'::('a'::(':'::(' '::('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('l'::('e'::('a'::(' '::('i'::('n'::('s'::('t'::('r'::('u'::('c'::('t'::('i'::('o'::('n'::[]))))))))))))))))))))))))))))))))
  | _ -> Ok None

(** val reg_of_ovar :
    instr_info -> var_i option -> (instr_info * error_msg, register option)
    result **)

let reg_of_ovar ii = function
| Some x0 -> Result.bind (fun r -> Ok (Some r)) (reg_of_var ii x0.v_var)
| None -> Ok None

(** val assemble_x86_opn :
    instr_info -> asm_op -> lval list -> pexpr list ->
    (instr_info * error_msg, asm_op * asm_arg list) result **)

let assemble_x86_opn ii op outx inx =
  Result.bind (fun is_lea0 ->
    match is_lea0 with
    | Some y ->
      let (y0, lea0) = y in
      let (sz, x) = y0 in
      Result.bind (fun r ->
        Result.bind (fun base ->
          Result.bind (fun offset ->
            Result.bind (fun scale ->
              let adr = { ad_disp = lea0.lea_disp; ad_base = base; ad_scale =
                scale; ad_offset = offset }
              in
              Ok ((LEA sz), ((Reg r) :: ((Adr adr) :: []))))
              (scale_of_z' ii lea0.lea_scale))
            (reg_of_ovar ii lea0.lea_offset)) (reg_of_ovar ii lea0.lea_base))
        (reg_of_var ii x.v_var)
    | None ->
      let id = instr_desc op in
      let max_imm = id.id_max_imm in
      Result.bind (fun asm_args ->
        let s = id.id_str_jas () in
        Result.bind (fun _ ->
          Result.bind (fun _ -> Ok (op, asm_args))
            (coq_assert
              ((&&)
                (check_sopn_args ii max_imm asm_args inx
                  (zip id.id_in id.id_tin))
                (check_sopn_dests ii max_imm asm_args outx
                  (zip id.id_out id.id_tout))) (ii, (Cerr_assembler
              (AsmErr_string
              ('a'::('s'::('s'::('e'::('m'::('b'::('l'::('e'::('_'::('x'::('8'::('6'::('_'::('o'::('p'::('n'::(':'::(' '::('c'::('a'::('n'::('n'::('o'::('t'::(' '::('c'::('h'::('e'::('c'::('k'::(','::(' '::('p'::('l'::('e'::('a'::('s'::('e'::(' '::('r'::('e'::('p'::('p'::('o'::('r'::('t'::[])))))))))))))))))))))))))))))))))))))))))))))))))))
          (coq_assert (id.id_check asm_args) (ii, (Cerr_assembler
            (AsmErr_string
            (append
              ('a'::('s'::('s'::('e'::('m'::('b'::('l'::('e'::('_'::('x'::('8'::('6'::('_'::('o'::('p'::('n'::(' '::(':'::(' '::('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('i'::('n'::('s'::('t'::('r'::('u'::('c'::('t'::('i'::('o'::('n'::(' '::('('::('c'::('h'::('e'::('c'::('k'::(')'::(' '::[])))))))))))))))))))))))))))))))))))))))))))))))
              s)))))) (assemble_x86_opn_aux ii op outx inx))
    (is_lea ii op outx inx)

(** val assemble_sopn :
    instr_info -> sopn -> lval list -> pexpr list -> (asm_op * asm_arg list)
    ciexec **)

let assemble_sopn ii op outx inx =
  match op with
  | Oset0 sz ->
    let op0 = if cmp_le wsize_cmp sz U64 then XOR sz else VPXOR sz in
    Result.bind (fun x ->
      assemble_x86_opn ii op0 outx ((Pvar x) :: ((Pvar x) :: [])))
      (match rev outx with
       | [] ->
         cierror ii (Cerr_assembler (AsmErr_string
           ('a'::('s'::('s'::('e'::('m'::('b'::('l'::('e'::('_'::('s'::('o'::('p'::('n'::(' '::('s'::('e'::('t'::('0'::(':'::(' '::('d'::('e'::('s'::('t'::('i'::('n'::('a'::('t'::('i'::('o'::('n'::(' '::('i'::('s'::(' '::('n'::('o'::('t'::(' '::('a'::(' '::('r'::('e'::('g'::('i'::('s'::('t'::('e'::('r'::[])))))))))))))))))))))))))))))))))))))))))))))))))))
       | l :: _ ->
         (match l with
          | Lvar x -> Ok x
          | _ ->
            cierror ii (Cerr_assembler (AsmErr_string
              ('a'::('s'::('s'::('e'::('m'::('b'::('l'::('e'::('_'::('s'::('o'::('p'::('n'::(' '::('s'::('e'::('t'::('0'::(':'::(' '::('d'::('e'::('s'::('t'::('i'::('n'::('a'::('t'::('i'::('o'::('n'::(' '::('i'::('s'::(' '::('n'::('o'::('t'::(' '::('a'::(' '::('r'::('e'::('g'::('i'::('s'::('t'::('e'::('r'::[])))))))))))))))))))))))))))))))))))))))))))))))))))))
  | Ox86MOVZX32 ->
    Result.bind (fun _ -> assemble_x86_opn ii (MOV U32) outx inx)
      (match outx with
       | [] ->
         cierror ii (Cerr_assembler (AsmErr_string
           ('a'::('s'::('s'::('e'::('m'::('b'::('l'::('e'::('_'::('s'::('o'::('p'::('n'::(' '::('O'::('x'::('8'::('6'::('M'::('O'::('V'::('Z'::('X'::('3'::('2'::(':'::(' '::('d'::('e'::('s'::('t'::('i'::('n'::('a'::('t'::('i'::('o'::('n'::(' '::('i'::('s'::(' '::('n'::('o'::('t'::(' '::('a'::(' '::('r'::('e'::('g'::('i'::('s'::('t'::('e'::('r'::[]))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
       | l :: l0 ->
         (match l with
          | Lvar x ->
            (match l0 with
             | [] -> Ok x
             | _ :: _ ->
               cierror ii (Cerr_assembler (AsmErr_string
                 ('a'::('s'::('s'::('e'::('m'::('b'::('l'::('e'::('_'::('s'::('o'::('p'::('n'::(' '::('O'::('x'::('8'::('6'::('M'::('O'::('V'::('Z'::('X'::('3'::('2'::(':'::(' '::('d'::('e'::('s'::('t'::('i'::('n'::('a'::('t'::('i'::('o'::('n'::(' '::('i'::('s'::(' '::('n'::('o'::('t'::(' '::('a'::(' '::('r'::('e'::('g'::('i'::('s'::('t'::('e'::('r'::[])))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
          | _ ->
            cierror ii (Cerr_assembler (AsmErr_string
              ('a'::('s'::('s'::('e'::('m'::('b'::('l'::('e'::('_'::('s'::('o'::('p'::('n'::(' '::('O'::('x'::('8'::('6'::('M'::('O'::('V'::('Z'::('X'::('3'::('2'::(':'::(' '::('d'::('e'::('s'::('t'::('i'::('n'::('a'::('t'::('i'::('o'::('n'::(' '::('i'::('s'::(' '::('n'::('o'::('t'::(' '::('a'::(' '::('r'::('e'::('g'::('i'::('s'::('t'::('e'::('r'::[]))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
  | Ox86 op0 -> assemble_x86_opn ii op0 outx inx
  | _ ->
    cierror ii (Cerr_assembler (AsmErr_string
      ('a'::('s'::('s'::('e'::('m'::('b'::('l'::('e'::('_'::('s'::('o'::('p'::('n'::(' '::(':'::(' '::('i'::('n'::('v'::('a'::('l'::('i'::('d'::(' '::('o'::('p'::[]))))))))))))))))))))))))))))
