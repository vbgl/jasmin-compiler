open BinNums
open String0
open Compiler_util
open Eqtype
open Expr
open Ssralg
open Ssrfun
open Strings
open Type
open Utils0
open Var0
open Word0
open Wsize
open X86_decl
open Xseq

val string_of_register : register -> char list

val string_of_rflag : rflag -> char list

val regs_strings : (char list * register) list

val xmm_regs_strings : (char list * xmm_register) list

val rflags_strings : (char list * rflag) list

val reg_of_string : char list -> register option

val xmm_reg_of_string : char list -> xmm_register option

val rflag_of_string : char list -> rflag option

val var_of_register : register -> Var.var

val var_of_flag : rflag -> Var.var

val register_of_var : Var.var -> register option

val xmm_register_of_var : Var.var -> xmm_register option

val invalid_rflag : char list -> asm_error

val invalid_register : char list -> asm_error

val rflag_of_var : instr_info -> Var.var -> rflag ciexec

val assemble_cond : instr_info -> pexpr -> condt ciexec

val reg_of_var : instr_info -> Var.var -> register ciexec

val reg_of_vars :
  instr_info -> var_i list -> (instr_info * error_msg, register list) result

val scale_of_z' : instr_info -> GRing.ComRing.sort -> scale ciexec

type ofs =
| Ofs_const of GRing.ComRing.sort
| Ofs_var of Var.var
| Ofs_mul of GRing.ComRing.sort * Var.var
| Ofs_add of GRing.ComRing.sort * Var.var * GRing.ComRing.sort
| Ofs_error

val addr_ofs : pexpr -> ofs

val addr_of_pexpr : instr_info -> register -> pexpr -> address ciexec

val xreg_of_var : instr_info -> Var.var -> asm_arg ciexec

val assemble_word :
  instr_info -> wsize -> wsize option -> pexpr -> asm_arg ciexec

val arg_of_pexpr :
  instr_info -> stype -> wsize option -> pexpr -> (instr_info * error_msg,
  asm_arg) result
