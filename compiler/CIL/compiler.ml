open BinNums
open Datatypes
open List0
open Allocation
open Array_expansion
open Compiler_util
open Constant_prop
open Dead_calls
open Dead_code
open Eqtype
open Expr
open Global
open Inline
open Linear
open Lowering
open Remove_globals
open Seq
open Stack_alloc
open Type
open Unrolling
open Utils0
open Var0
open X86_gen
open X86_sem

(** val unroll1 : prog -> prog cfexec **)

let unroll1 p =
  let p0 = unroll_prog p in let p1 = const_prop_prog p0 in dead_code_prog p1

(** val unroll : nat -> prog -> prog cfexec **)

let rec unroll n p =
  match n with
  | O -> cferror Ferr_loop
  | S n0 ->
    Result.bind (fun p' ->
      if eq_op prog_eqType (Obj.magic p) (Obj.magic p')
      then cfok p
      else unroll n0 p') (unroll1 p)

(** val unroll_loop : prog -> prog cfexec **)

let unroll_loop p =
  unroll Loop.nb p

type compiler_step =
| Typing
| ParamsExpansion
| Inlining
| RemoveUnusedFunction
| Unrolling
| Splitting
| AllocInlineAssgn
| DeadCode_AllocInlineAssgn
| ShareStackVariable
| DeadCode_ShareStackVariable
| RegArrayExpansion
| RemoveArrInit
| RemoveGlobal
| LowerInstruction
| RegAllocation
| DeadCode_RegAllocation
| StackAllocation
| Linearisation
| Assembly

type compiler_params = { rename_fd : (instr_info -> funname -> fundef ->
                                     fundef);
                         expand_fd : (funname -> fundef -> fundef);
                         var_alloc_fd : (funname -> fundef -> fundef);
                         share_stk_fd : (funname -> fundef -> fundef);
                         lowering_vars : fresh_vars;
                         inline_var : (Var.var -> bool);
                         is_var_in_memory : (var_i -> bool);
                         reg_alloc_fd : (funname -> fundef -> fundef);
                         stk_alloc_fd : (fun_decl ->
                                        ((coq_Z * Equality.sort) * (Var.var * coq_Z)
                                        list) * (Var.var
                                        list * Stack_alloc.saved_stack));
                         print_prog : (compiler_step -> prog -> prog);
                         print_linear : (lprog -> lprog);
                         warning : (instr_info -> warning_msg -> instr_info);
                         lowering_opt : lowering_options;
                         is_glob : (Var.var -> bool);
                         fresh_id : (glob_decl list -> Var.var ->
                                    Equality.sort) }

(** val rename_fd :
    compiler_params -> instr_info -> funname -> fundef -> fundef **)

let rename_fd x = x.rename_fd

(** val expand_fd : compiler_params -> funname -> fundef -> fundef **)

let expand_fd x = x.expand_fd

(** val var_alloc_fd : compiler_params -> funname -> fundef -> fundef **)

let var_alloc_fd x = x.var_alloc_fd

(** val share_stk_fd : compiler_params -> funname -> fundef -> fundef **)

let share_stk_fd x = x.share_stk_fd

(** val lowering_vars : compiler_params -> fresh_vars **)

let lowering_vars x = x.lowering_vars

(** val inline_var : compiler_params -> Var.var -> bool **)

let inline_var x = x.inline_var

(** val is_var_in_memory : compiler_params -> var_i -> bool **)

let is_var_in_memory x = x.is_var_in_memory

(** val reg_alloc_fd : compiler_params -> funname -> fundef -> fundef **)

let reg_alloc_fd x = x.reg_alloc_fd

(** val stk_alloc_fd :
    compiler_params -> fun_decl ->
    ((coq_Z * Equality.sort) * (Var.var * coq_Z) list) * (Var.var
    list * Stack_alloc.saved_stack) **)

let stk_alloc_fd x = x.stk_alloc_fd

(** val print_prog : compiler_params -> compiler_step -> prog -> prog **)

let print_prog x = x.print_prog

(** val print_linear : compiler_params -> lprog -> lprog **)

let print_linear x = x.print_linear

(** val warning :
    compiler_params -> instr_info -> warning_msg -> instr_info **)

let warning x = x.warning

(** val lowering_opt : compiler_params -> lowering_options **)

let lowering_opt x = x.lowering_opt

(** val is_glob : compiler_params -> Var.var -> bool **)

let is_glob x = x.is_glob

(** val fresh_id :
    compiler_params -> glob_decl list -> Var.var -> Equality.sort **)

let fresh_id x = x.fresh_id

(** val expand_prog : compiler_params -> prog -> prog **)

let expand_prog cparams p =
  { p_globs = p.p_globs; p_funcs =
    (List0.map (fun f -> ((fst f), (cparams.expand_fd (fst f) (snd f))))
      p.p_funcs) }

(** val var_alloc_prog : compiler_params -> prog -> prog **)

let var_alloc_prog cparams p =
  { p_globs = p.p_globs; p_funcs =
    (List0.map (fun f -> ((fst f), (cparams.var_alloc_fd (fst f) (snd f))))
      p.p_funcs) }

(** val share_stack_prog : compiler_params -> prog -> prog **)

let share_stack_prog cparams p =
  { p_globs = p.p_globs; p_funcs =
    (List0.map (fun f -> ((fst f), (cparams.share_stk_fd (fst f) (snd f))))
      p.p_funcs) }

(** val reg_alloc_prog : compiler_params -> prog -> prog **)

let reg_alloc_prog cparams p =
  { p_globs = p.p_globs; p_funcs =
    (List0.map (fun f -> ((fst f), (cparams.reg_alloc_fd (fst f) (snd f))))
      p.p_funcs) }

(** val compile_prog :
    compiler_params -> funname list -> prog -> (fun_error, glob_decl
    list * lprog) result **)

let compile_prog cparams entries p =
  Result.bind (fun p0 ->
    let p1 = cparams.print_prog Inlining p0 in
    Result.bind (fun p2 ->
      let p3 = cparams.print_prog RemoveUnusedFunction p2 in
      Result.bind (fun p4 ->
        let p5 = cparams.print_prog Unrolling p4 in
        let p6 = const_prop_prog p5 in
        let p7 = cparams.print_prog Unrolling p6 in
        let pv = var_alloc_prog cparams p7 in
        let pv0 = cparams.print_prog AllocInlineAssgn pv in
        Result.bind (fun _ ->
          Result.bind (fun pv1 ->
            let pv2 = cparams.print_prog DeadCode_AllocInlineAssgn pv1 in
            let ps = share_stack_prog cparams pv2 in
            let ps0 = cparams.print_prog ShareStackVariable ps in
            Result.bind (fun _ ->
              Result.bind (fun ps1 ->
                let ps2 = cparams.print_prog DeadCode_ShareStackVariable ps1
                in
                let pr = remove_init_prog ps2 in
                let pr0 = cparams.print_prog RemoveArrInit pr in
                let pe = expand_prog cparams pr0 in
                let pe0 = cparams.print_prog RegArrayExpansion pe in
                Result.bind (fun _ ->
                  Result.bind (fun pg ->
                    let pg0 = cparams.print_prog RemoveGlobal pg in
                    if fvars_correct cparams.lowering_vars pg0.p_funcs
                    then let pl =
                           lower_prog cparams.lowering_opt cparams.warning
                             cparams.lowering_vars cparams.is_var_in_memory
                             pg0
                         in
                         let pl0 = cparams.print_prog LowerInstruction pl in
                         let pa = reg_alloc_prog cparams pl0 in
                         let pa0 = cparams.print_prog RegAllocation pa in
                         Result.bind (fun _ ->
                           Result.bind (fun pd ->
                             let pd0 =
                               cparams.print_prog DeadCode_RegAllocation pd
                             in
                             Result.bind (fun ps3 ->
                               Result.bind (fun pl1 ->
                                 let pl2 = cparams.print_linear pl1 in
                                 cfok (pd0.p_globs, pl2)) (linear_prog ps3))
                               (alloc_prog cparams.stk_alloc_fd pd0))
                             (dead_code_prog pa0))
                           (CheckAllocReg.check_prog pl0 pa0)
                    else cferror Ferr_lowering)
                    (remove_glob_prog cparams.is_glob cparams.fresh_id pe0))
                  (CheckExpansion.check_prog pr0 pe0)) (dead_code_prog ps0))
              (CheckAllocReg.check_prog pv2 ps0)) (dead_code_prog pv0))
          (CheckAllocReg.check_prog p7 pv0)) (unroll Loop.nb p3))
      (dead_calls_err_seq entries p1))
    (inline_prog_err cparams.inline_var cparams.rename_fd p)

(** val check_signature : prog -> lprog -> funname -> bool **)

let check_signature p lp fn =
  match get_fundef lp fn with
  | Some fd' ->
    (match get_fundef p.p_funcs fn with
     | Some fd ->
       eq_op
         (prod_eqType (seq_eqType stype_eqType) (seq_eqType stype_eqType))
         (Obj.magic signature_of_fundef fd)
         (Obj.magic signature_of_lfundef fd')
     | None -> true)
  | None -> true

(** val compile_prog_to_x86 :
    compiler_params -> funname list -> prog -> (fun_error, glob_decl
    list * xprog) result **)

let compile_prog_to_x86 cparams entries p =
  Result.bind (fun lp ->
    Result.bind (fun _ ->
      Result.bind (fun lx -> Ok ((fst lp), lx)) (assemble_prog (snd lp)))
      (coq_assert (all (check_signature p (snd lp)) entries) Ferr_lowering))
    (compile_prog cparams entries p)
