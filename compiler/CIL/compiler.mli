open BinNums
open Datatypes
open List0
open Allocation
open Array_expansion
open Compiler_util
open Constant_prop
open Dead_calls
open Dead_code
open Eqtype
open Expr
open Global
open Inline
open Linear
open Lowering
open Remove_globals
open Seq
open Stack_alloc
open Type
open Unrolling
open Utils0
open Var0
open X86_gen
open X86_sem

val unroll1 : prog -> prog cfexec

val unroll : nat -> prog -> prog cfexec

val unroll_loop : prog -> prog cfexec

type compiler_step =
| Typing
| ParamsExpansion
| Inlining
| RemoveUnusedFunction
| Unrolling
| Splitting
| AllocInlineAssgn
| DeadCode_AllocInlineAssgn
| ShareStackVariable
| DeadCode_ShareStackVariable
| RegArrayExpansion
| RemoveArrInit
| RemoveGlobal
| LowerInstruction
| RegAllocation
| DeadCode_RegAllocation
| StackAllocation
| Linearisation
| Assembly

type compiler_params = { rename_fd : (instr_info -> funname -> fundef ->
                                     fundef);
                         expand_fd : (funname -> fundef -> fundef);
                         var_alloc_fd : (funname -> fundef -> fundef);
                         share_stk_fd : (funname -> fundef -> fundef);
                         lowering_vars : fresh_vars;
                         inline_var : (Var.var -> bool);
                         is_var_in_memory : (var_i -> bool);
                         reg_alloc_fd : (funname -> fundef -> fundef);
                         stk_alloc_fd : (fun_decl ->
                                        ((coq_Z * Equality.sort) * (Var.var * coq_Z)
                                        list) * (Var.var
                                        list * Stack_alloc.saved_stack));
                         print_prog : (compiler_step -> prog -> prog);
                         print_linear : (lprog -> lprog);
                         warning : (instr_info -> warning_msg -> instr_info);
                         lowering_opt : lowering_options;
                         is_glob : (Var.var -> bool);
                         fresh_id : (glob_decl list -> Var.var ->
                                    Equality.sort) }

val rename_fd : compiler_params -> instr_info -> funname -> fundef -> fundef

val expand_fd : compiler_params -> funname -> fundef -> fundef

val var_alloc_fd : compiler_params -> funname -> fundef -> fundef

val share_stk_fd : compiler_params -> funname -> fundef -> fundef

val lowering_vars : compiler_params -> fresh_vars

val inline_var : compiler_params -> Var.var -> bool

val is_var_in_memory : compiler_params -> var_i -> bool

val reg_alloc_fd : compiler_params -> funname -> fundef -> fundef

val stk_alloc_fd :
  compiler_params -> fun_decl -> ((coq_Z * Equality.sort) * (Var.var * coq_Z)
  list) * (Var.var list * Stack_alloc.saved_stack)

val print_prog : compiler_params -> compiler_step -> prog -> prog

val print_linear : compiler_params -> lprog -> lprog

val warning : compiler_params -> instr_info -> warning_msg -> instr_info

val lowering_opt : compiler_params -> lowering_options

val is_glob : compiler_params -> Var.var -> bool

val fresh_id : compiler_params -> glob_decl list -> Var.var -> Equality.sort

val expand_prog : compiler_params -> prog -> prog

val var_alloc_prog : compiler_params -> prog -> prog

val share_stack_prog : compiler_params -> prog -> prog

val reg_alloc_prog : compiler_params -> prog -> prog

val compile_prog :
  compiler_params -> funname list -> prog -> (fun_error, glob_decl
  list * lprog) result

val check_signature : prog -> lprog -> funname -> bool

val compile_prog_to_x86 :
  compiler_params -> funname list -> prog -> (fun_error, glob_decl
  list * xprog) result
