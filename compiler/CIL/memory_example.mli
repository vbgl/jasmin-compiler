open BinInt
open BinNums
open Bool
open Datatypes
open Sumbool
open Eqtype
open Gen_map
open Memory_model
open Seq
open SsrZ
open Ssralg
open Ssrbool
open Ssrfun
open Utils0
open Word0
open Wsize

type __ = Obj.t

module Align :
 sig
  val coq_A : alignment
 end

val add : GRing.ComRing.sort -> coq_Z -> GRing.Zmodule.sort

val sub : GRing.ComRing.sort -> GRing.ComRing.sort -> coq_Z

module MemoryI :
 MemoryT
