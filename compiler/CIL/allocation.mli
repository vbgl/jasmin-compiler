open Bool
open Datatypes
open Compiler_util
open Eqtype
open Expr
open Gen_map
open Global
open Sem
open Seq
open SsrZ
open Ssrbool
open Ssreflect
open Ssrfun
open Strings
open Type
open Utils0
open Var0
open Wsize

module type CheckB =
 sig
  module M :
   sig
    type t

    val empty : t

    val merge : t -> t -> t

    val incl : t -> t -> bool
   end

  val check_e : pexpr -> pexpr -> M.t -> M.t cexec

  val check_lval : (stype * pexpr) option -> lval -> lval -> M.t -> M.t cexec
 end

val salloc : char list

module MakeCheckAlloc :
 functor (C:CheckB) ->
 sig
  val loop :
    instr_info -> (C.M.t -> C.M.t ciexec) -> nat -> C.M.t -> C.M.t ciexec

  val loop2 :
    instr_info -> (C.M.t -> (C.M.t * C.M.t) ciexec) -> nat -> C.M.t -> C.M.t
    ciexec

  val check_e_error : error_msg

  val cmd2_error : error_msg

  val check_es :
    pexpr list -> pexpr list -> C.M.t -> (error_msg, C.M.t) result

  val check_lvals :
    lval list -> lval list -> C.M.t -> (error_msg, C.M.t) result

  val check_var : var_i -> var_i -> C.M.t -> C.M.t cexec

  val check_vars :
    var_i list -> var_i list -> C.M.t -> (error_msg, C.M.t) result

  val check_i : instr_info -> instr_r -> instr_r -> C.M.t -> C.M.t ciexec

  val check_I :
    instr -> instr -> C.M.t -> (instr_info * error_msg, C.M.t) result

  val check_cmd :
    instr_info -> instr list -> instr list -> C.M.t ->
    (instr_info * error_msg, C.M.t) result

  val check_fundef :
    (funname * fundef) -> (funname * fundef) -> unit -> unit cfexec

  val check_prog_aux : prog -> prog -> (fun_error, unit) result

  val check_prog : prog -> prog -> (fun_error, unit) result
 end

module MakeMalloc :
 functor (M:MAP) ->
 sig
  type t_ = { mvar : Equality.sort M.t; mid : Equality.sort Ms.t }

  val mvar : t_ -> Equality.sort M.t

  val mid : t_ -> Equality.sort Ms.t

  type t = t_

  val get : t -> Equality.sort -> Equality.sort option

  val rm_id : t -> Equality.sort -> Equality.sort M.t

  val rm_x : t -> Equality.sort -> Equality.sort Ms.Map.t

  val remove : t -> Equality.sort -> t_

  val set : t -> Equality.sort -> Equality.sort -> t_

  val empty : t_

  val merge : t_ -> t -> t

  val incl : t_ -> t -> bool

  val inclP : t -> t -> reflect
 end

module CBAreg :
 sig
  module M :
   sig
    module Mv :
     sig
      val oget : Sv.t Mvar.t -> Equality.sort -> Sv.t

      type t_ = { mvar : Var.var Mvar.t; mid : Sv.t Mvar.t }

      val mvar : t_ -> Var.var Mvar.t

      val mid : t_ -> Sv.t Mvar.t

      type t = t_

      val get : t -> Var.var -> Var.var option

      val rm_id : t -> Equality.sort -> Var.var Mvar.t

      val ms_upd :
        Sv.t Mvar.t -> (Sv.t -> Sv.t) -> Equality.sort -> Sv.t Mvar.Map.t

      val rm_x : t -> Equality.sort -> Sv.t Mvar.Map.t

      val remove : t -> Equality.sort -> t_

      val set : t -> Equality.sort -> Equality.sort -> t_

      val add : t_ -> Equality.sort -> Var.var -> t_

      val empty : t_
     end

    val bool_dec : bool -> bool

    val vsubtype : Var.var -> Var.var -> bool

    val vsubtypeP : Var.var -> Var.var -> bool

    type t_ = { mv : Mv.t; mset : Sv.t }

    val mv : t_ -> Mv.t

    val mset : t_ -> Sv.t

    type t = t_

    val get : t -> Var.var -> Var.var option

    val set : t_ -> Var.var -> Var.var -> t_

    val add : t_ -> Var.var -> Var.var -> t_

    val addc : t_ -> Var.var -> Var.var -> t_

    val empty_s : Sv.t -> t_

    val empty : t_

    val merge_aux : t_ -> t_ -> Equality.sort Mvar.t

    val merge : t_ -> t_ -> t_

    val remove : t_ -> Equality.sort -> t_

    val incl : t_ -> t_ -> bool

    val inclP : t -> t_ -> reflect
   end

  val check_v : var_i -> var_i -> M.t -> M.t cexec

  val check_e : pexpr -> pexpr -> M.t -> M.t cexec

  val check_var : Var.var -> Var.var -> M.t_ -> M.t cexec

  val check_varc : var_i -> var_i -> M.t_ -> M.t cexec

  val is_Pvar : (stype * pexpr) option -> (stype * var_i) option

  val check_lval : (stype * pexpr) option -> lval -> lval -> M.t -> M.t cexec
 end

module CheckAllocReg :
 sig
  val loop :
    instr_info -> (CBAreg.M.t -> CBAreg.M.t ciexec) -> nat -> CBAreg.M.t ->
    CBAreg.M.t ciexec

  val loop2 :
    instr_info -> (CBAreg.M.t -> (CBAreg.M.t * CBAreg.M.t) ciexec) -> nat ->
    CBAreg.M.t -> CBAreg.M.t ciexec

  val check_e_error : error_msg

  val cmd2_error : error_msg

  val check_es :
    pexpr list -> pexpr list -> CBAreg.M.t -> (error_msg, CBAreg.M.t) result

  val check_lvals :
    lval list -> lval list -> CBAreg.M.t -> (error_msg, CBAreg.M.t) result

  val check_var : var_i -> var_i -> CBAreg.M.t -> CBAreg.M.t cexec

  val check_vars :
    var_i list -> var_i list -> CBAreg.M.t -> (error_msg, CBAreg.M.t) result

  val check_i :
    instr_info -> instr_r -> instr_r -> CBAreg.M.t -> CBAreg.M.t ciexec

  val check_I :
    instr -> instr -> CBAreg.M.t -> (instr_info * error_msg, CBAreg.M.t)
    result

  val check_cmd :
    instr_info -> instr list -> instr list -> CBAreg.M.t ->
    (instr_info * error_msg, CBAreg.M.t) result

  val check_fundef :
    (funname * fundef) -> (funname * fundef) -> unit -> unit cfexec

  val check_prog_aux : prog -> prog -> (fun_error, unit) result

  val check_prog : prog -> prog -> (fun_error, unit) result
 end
