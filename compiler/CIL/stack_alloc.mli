open BinInt
open BinNums
open Datatypes
open Compiler_util
open Eqtype
open Expr
open Low_memory
open Memory_model
open Seq
open Type
open Utils0
open Var0
open Word0
open Wsize

type saved_stack =
| SavedStackNone
| SavedStackReg of Var.var
| SavedStackStk of coq_Z

type sfundef = { sf_iinfo : instr_info; sf_stk_sz : coq_Z;
                 sf_stk_id : Equality.sort; sf_tyin : stype list;
                 sf_params : var_i list; sf_body : instr list;
                 sf_tyout : stype list; sf_res : var_i list;
                 sf_extra : (Var.var list * saved_stack) }

val sf_stk_sz : sfundef -> coq_Z

val sf_stk_id : sfundef -> Equality.sort

val sf_tyin : sfundef -> stype list

val sf_params : sfundef -> var_i list

val sf_body : sfundef -> instr list

val sf_tyout : sfundef -> stype list

val sf_res : sfundef -> var_i list

val sf_extra : sfundef -> Var.var list * saved_stack

type sprog = (funname * sfundef) list

type map = coq_Z Mvar.t * Equality.sort

val size_of : stype -> coq_Z cexec

val aligned_for : stype -> coq_Z -> bool

val init_map :
  coq_Z -> Equality.sort -> (Var.var * coq_Z) list -> (error_msg, coq_Z
  Mvar.t * Equality.sort) result

val is_in_stk : map -> Var.var -> bool

val vstk : map -> Var.var

val is_vstk : map -> Var.var -> bool

val check_var : map -> var_i -> bool

val cast_w : wsize -> pexpr -> pexpr

val cast_ptr : pexpr -> pexpr

val cast_const : coq_Z -> pexpr

val mul : pexpr -> pexpr -> pexpr

val add : pexpr -> pexpr -> pexpr

val cast_word : pexpr -> pexpr

val stk_not_fresh : 'a1 cexec

val not_a_word_v : 'a1 cexec

val not_aligned : 'a1 cexec

val invalid_var : 'a1 cexec

val mk_ofs : wsize -> pexpr -> coq_Z -> pexpr

val alloc_e : map -> pexpr -> (error_msg, pexpr) result

val alloc_lval : map -> lval -> Equality.sort -> (error_msg, lval) result

val bad_lval_number : error_msg

val alloc_i : map -> instr -> (instr_info * error_msg, instr) result

val add_err_fun : funname -> 'a1 cexec -> (fun_error, 'a1) result

val alloc_fd :
  (fun_decl -> ((coq_Z * Equality.sort) * (Var.var * coq_Z) list) * (Var.var
  list * saved_stack)) -> fun_decl -> (fun_error, funname * sfundef) result

val alloc_prog :
  (fun_decl -> ((coq_Z * Equality.sort) * (Var.var * coq_Z) list) * (Var.var
  list * saved_stack)) -> prog -> (fun_error, (funname * sfundef) list) result
