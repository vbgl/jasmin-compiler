open BinNums
open Datatypes
open Compiler_util
open Eqtype
open Expr
open Global
open Seq
open SsrZ
open Type
open Utils0
open Var0
open Wsize

(** val myfind : ('a1 -> 'a2 option) -> 'a1 list -> 'a2 option **)

let rec myfind f = function
| [] -> None
| a :: l0 ->
  let fa = f a in (match fa with
                   | Some _ -> fa
                   | None -> myfind f l0)

(** val find_glob :
    instr_info -> var_i -> glob_decl list -> wsize -> coq_Z -> (fun_error,
    global) result **)

let find_glob ii xi gd ws z =
  let test = fun gv ->
    if (&&)
         (eq_op wsize_eqType (Obj.magic ws)
           (Obj.magic size_of_global (fst gv)))
         (eq_op coq_Z_eqType (Obj.magic z) (snd gv))
    then Some (fst gv)
    else None
  in
  (match myfind (Obj.magic test) gd with
   | Some g -> Ok g
   | None -> cferror (Ferr_remove_glob (ii, xi)))

(** val add_glob :
    (glob_decl list -> Var.var -> Equality.sort) -> instr_info -> Var.var ->
    glob_decl list -> wsize -> coq_Z -> (fun_error, glob_decl list) result **)

let add_glob fresh_id ii x gd ws z =
  let test = fun gv ->
    (&&)
      (eq_op wsize_eqType (Obj.magic ws) (Obj.magic size_of_global (fst gv)))
      (eq_op coq_Z_eqType (Obj.magic z) (snd gv))
  in
  if has (Obj.magic test) gd
  then Ok gd
  else let g = { size_of_global = ws; ident_of_global = (fresh_id gd x) } in
       if has (fun g' ->
            eq_op global_eqType (fst (Obj.magic g')) (Obj.magic g)) gd
       then cferror (Ferr_remove_glob_dup (ii, g))
       else Ok ((g, z) :: gd)

(** val extend_glob_i :
    (Var.var -> bool) -> (glob_decl list -> Var.var -> Equality.sort) ->
    instr -> glob_decl list -> (fun_error, glob_decl list) result **)

let rec extend_glob_i is_glob fresh_id i gd =
  let MkI (ii, i0) = i in
  (match i0 with
   | Cassgn (lv, _, _, e) ->
     (match lv with
      | Lvar xi ->
        let x = xi.v_var in
        if is_glob x
        then (match e with
              | Papp1 (s, p) ->
                (match s with
                 | Oword_of_int ws ->
                   (match p with
                    | Pconst z -> add_glob fresh_id ii x gd ws z
                    | _ -> cferror (Ferr_remove_glob (ii, xi)))
                 | _ -> cferror (Ferr_remove_glob (ii, xi)))
              | _ -> cferror (Ferr_remove_glob (ii, xi)))
        else Ok gd
      | _ -> Ok gd)
   | Cif (_, c1, c2) ->
     Result.bind (fun gd0 -> foldM (extend_glob_i is_glob fresh_id) gd0 c2)
       (foldM (extend_glob_i is_glob fresh_id) gd c1)
   | Cfor (_, _, c) -> foldM (extend_glob_i is_glob fresh_id) gd c
   | Cwhile (_, c1, _, c2) ->
     Result.bind (fun gd0 -> foldM (extend_glob_i is_glob fresh_id) gd0 c2)
       (foldM (extend_glob_i is_glob fresh_id) gd c1)
   | _ -> Ok gd)

(** val extend_glob_prog :
    (Var.var -> bool) -> (glob_decl list -> Var.var -> Equality.sort) -> prog
    -> (fun_error, glob_decl list) result **)

let extend_glob_prog is_glob fresh_id p =
  foldM (fun f gd ->
    foldM (extend_glob_i is_glob fresh_id) gd (snd f).f_body) p.p_globs
    p.p_funcs

(** val remove_glob_e :
    (Var.var -> bool) -> instr_info -> global Mvar.t -> pexpr -> (fun_error,
    pexpr) result **)

let rec remove_glob_e is_glob ii env e = match e with
| Pvar xi ->
  let x = xi.v_var in
  if is_glob x
  then (match Mvar.get env (Obj.magic x) with
        | Some g -> Ok (Pglobal g)
        | None -> cferror (Ferr_remove_glob (ii, xi)))
  else Ok e
| Pget (ws, xi, e0) ->
  let x = xi.v_var in
  if is_glob x
  then cferror (Ferr_remove_glob (ii, xi))
  else Result.bind (fun e1 -> Ok (Pget (ws, xi, e1)))
         (remove_glob_e is_glob ii env e0)
| Pload (ws, xi, e0) ->
  let x = xi.v_var in
  if is_glob x
  then cferror (Ferr_remove_glob (ii, xi))
  else Result.bind (fun e1 -> Ok (Pload (ws, xi, e1)))
         (remove_glob_e is_glob ii env e0)
| Papp1 (o, e0) ->
  Result.bind (fun e1 -> Ok (Papp1 (o, e1))) (remove_glob_e is_glob ii env e0)
| Papp2 (o, e1, e2) ->
  Result.bind (fun e3 ->
    Result.bind (fun e4 -> Ok (Papp2 (o, e3, e4)))
      (remove_glob_e is_glob ii env e2)) (remove_glob_e is_glob ii env e1)
| PappN (op, es) ->
  Result.bind (fun es0 -> Ok (PappN (op, es0)))
    (mapM (remove_glob_e is_glob ii env) es)
| Pif (t0, e0, e1, e2) ->
  Result.bind (fun e3 ->
    Result.bind (fun e4 ->
      Result.bind (fun e5 -> Ok (Pif (t0, e3, e4, e5)))
        (remove_glob_e is_glob ii env e2)) (remove_glob_e is_glob ii env e1))
    (remove_glob_e is_glob ii env e0)
| _ -> Ok e

(** val remove_glob_lv :
    (Var.var -> bool) -> instr_info -> global Mvar.t -> lval -> (fun_error,
    lval) result **)

let remove_glob_lv is_glob ii env lv = match lv with
| Lnone (_, _) -> Ok lv
| Lvar xi ->
  let x = xi.v_var in
  if is_glob x then cferror (Ferr_remove_glob (ii, xi)) else Ok lv
| Lmem (ws, xi, e) ->
  let x = xi.v_var in
  if is_glob x
  then cferror (Ferr_remove_glob (ii, xi))
  else Result.bind (fun e0 -> Ok (Lmem (ws, xi, e0)))
         (remove_glob_e is_glob ii env e)
| Laset (ws, xi, e) ->
  let x = xi.v_var in
  if is_glob x
  then cferror (Ferr_remove_glob (ii, xi))
  else Result.bind (fun e0 -> Ok (Laset (ws, xi, e0)))
         (remove_glob_e is_glob ii env e)

(** val remove_glob :
    (global Mvar.t -> instr -> (global Mvar.t * instr list) cfexec) -> global
    Mvar.t -> instr list -> (global Mvar.t * instr list) cfexec **)

let rec remove_glob remove_glob_i0 e = function
| [] -> Ok (e, [])
| i :: c0 ->
  Result.bind (fun envi ->
    Result.bind (fun envc -> Ok ((fst envc), (app (snd envi) (snd envc))))
      (remove_glob remove_glob_i0 (fst envi) c0)) (remove_glob_i0 e i)

(** val merge_glob :
    Var.var -> global option -> global option -> global option **)

let merge_glob _ o1 o2 =
  match o1 with
  | Some g1 ->
    (match o2 with
     | Some g2 ->
       if eq_op global_eqType (Obj.magic g1) (Obj.magic g2) then o1 else None
     | None -> None)
  | None -> None

(** val coq_Mincl : global Mvar.t -> global Mvar.t -> bool **)

let coq_Mincl m1 m2 =
  all (fun xg ->
    match Mvar.get m2 (fst xg) with
    | Some g' -> eq_op global_eqType (snd (Obj.magic xg)) (Obj.magic g')
    | None -> false) (Mvar.elements m1)

(** val merge_env : global Mvar.t -> global Mvar.t -> global Mvar.t **)

let merge_env env1 env2 =
  Mvar.map2 (Obj.magic merge_glob) env1 env2

(** val loop :
    funname -> (global Mvar.t -> (global Mvar.t * instr list) cfexec) -> nat
    -> global Mvar.t -> (global Mvar.t * instr list) cfexec **)

let rec loop fn check_c n m =
  match n with
  | O ->
    cferror (Ferr_fun (fn, (Cerr_Loop
      ('r'::('e'::('m'::('o'::('v'::('e'::('_'::('g'::('l'::('o'::('b'::[]))))))))))))))
  | S n0 ->
    Result.bind (fun m' ->
      if coq_Mincl m (fst m')
      then Ok (m, (snd m'))
      else loop fn check_c n0 (merge_env m (fst m'))) (check_c m)

type check2_r =
| Check2_r of pexpr * (global Mvar.t * instr list)
   * (global Mvar.t * instr list)

type loop2_r =
| Loop2_r of pexpr * instr list * instr list * global Mvar.t

(** val loop2 :
    funname -> (global Mvar.t -> check2_r cfexec) -> nat -> global Mvar.t ->
    loop2_r cfexec **)

let rec loop2 fn check_c2 n m =
  match n with
  | O ->
    cferror (Ferr_fun (fn, (Cerr_Loop
      ('r'::('e'::('m'::('o'::('v'::('e'::('_'::('g'::('l'::('o'::('b'::[]))))))))))))))
  | S n0 ->
    Result.bind (fun cr ->
      let Check2_r (e, p, p0) = cr in
      let (m1, c1) = p in
      let (m2, c2) = p0 in
      if coq_Mincl m m2
      then Ok (Loop2_r (e, c1, c2, m1))
      else loop2 fn check_c2 n0 (merge_env m m2)) (check_c2 m)

(** val remove_glob_i :
    (Var.var -> bool) -> glob_decl list -> funname -> global Mvar.t -> instr
    -> (global Mvar.t * instr list) cfexec **)

let rec remove_glob_i is_glob gd fn env = function
| MkI (ii, i0) ->
  (match i0 with
   | Cassgn (lv, tag, ty, e) ->
     Result.bind (fun e0 ->
       match lv with
       | Lvar xi ->
         let x = xi.v_var in
         if is_glob x
         then (match e0 with
               | Papp1 (s, p) ->
                 (match s with
                  | Oword_of_int ws ->
                    (match p with
                     | Pconst z ->
                       if (&&)
                            (eq_op stype_eqType (Obj.magic ty)
                              (Obj.magic (Coq_sword ws)))
                            (eq_op stype_eqType (Obj.magic Var.vtype x)
                              (Obj.magic (Coq_sword ws)))
                       then Result.bind (fun g -> Ok
                              ((Mvar.set env (Obj.magic x) g), []))
                              (find_glob ii xi gd ws z)
                       else cferror (Ferr_remove_glob (ii, xi))
                     | _ -> cferror (Ferr_remove_glob (ii, xi)))
                  | _ -> cferror (Ferr_remove_glob (ii, xi)))
               | _ -> cferror (Ferr_remove_glob (ii, xi)))
         else Result.bind (fun lv0 -> Ok (env, ((MkI (ii, (Cassgn (lv0, tag,
                ty, e0)))) :: []))) (remove_glob_lv is_glob ii env lv)
       | _ ->
         Result.bind (fun lv0 -> Ok (env, ((MkI (ii, (Cassgn (lv0, tag, ty,
           e0)))) :: []))) (remove_glob_lv is_glob ii env lv))
       (remove_glob_e is_glob ii env e)
   | Copn (lvs, tag, o, es) ->
     Result.bind (fun lvs0 ->
       Result.bind (fun es0 -> Ok (env, ((MkI (ii, (Copn (lvs0, tag, o,
         es0)))) :: []))) (mapM (remove_glob_e is_glob ii env) es))
       (mapM (remove_glob_lv is_glob ii env) lvs)
   | Cif (e, c1, c2) ->
     Result.bind (fun e0 ->
       Result.bind (fun envc1 ->
         let env1 = fst envc1 in
         let c3 = snd envc1 in
         Result.bind (fun envc2 ->
           let env2 = fst envc2 in
           let c4 = snd envc2 in
           let env0 = merge_env env1 env2 in
           Ok (env0, ((MkI (ii, (Cif (e0, c3, c4)))) :: [])))
           (remove_glob (remove_glob_i is_glob gd fn) env c2))
         (remove_glob (remove_glob_i is_glob gd fn) env c1))
       (remove_glob_e is_glob ii env e)
   | Cfor (xi, r, c) ->
     let (p, e2) = r in
     let (d, e1) = p in
     if is_glob xi.v_var
     then cferror (Ferr_remove_glob (ii, xi))
     else Result.bind (fun e3 ->
            Result.bind (fun e4 ->
              let check_c = fun env0 ->
                remove_glob (remove_glob_i is_glob gd fn) env0 c
              in
              Result.bind (fun envc ->
                let (env0, c0) = envc in
                Ok (env0, ((MkI (ii, (Cfor (xi, ((d, e3), e4), c0)))) :: [])))
                (loop fn check_c Loop.nb env))
              (remove_glob_e is_glob ii env e2))
            (remove_glob_e is_glob ii env e1)
   | Cwhile (a, c1, e, c2) ->
     let check_c = fun env0 ->
       Result.bind (fun envc1 ->
         let env1 = fst envc1 in
         Result.bind (fun e0 ->
           Result.bind (fun envc2 -> Ok (Check2_r (e0, envc1, envc2)))
             (remove_glob (remove_glob_i is_glob gd fn) env1 c2))
           (remove_glob_e is_glob ii env1 e))
         (remove_glob (remove_glob_i is_glob gd fn) env0 c1)
     in
     Result.bind (fun lr ->
       let Loop2_r (e0, c3, c4, env0) = lr in
       Ok (env0, ((MkI (ii, (Cwhile (a, c3, e0, c4)))) :: [])))
       (loop2 fn check_c Loop.nb env)
   | Ccall (i1, lvs, fn0, es) ->
     Result.bind (fun lvs0 ->
       Result.bind (fun es0 -> Ok (env, ((MkI (ii, (Ccall (i1, lvs0, fn0,
         es0)))) :: []))) (mapM (remove_glob_e is_glob ii env) es))
       (mapM (remove_glob_lv is_glob ii env) lvs))

(** val remove_glob_fundef :
    (Var.var -> bool) -> glob_decl list -> (funname * fundef) -> (fun_error,
    funname * fundef) result **)

let remove_glob_fundef is_glob gd = function
| (fn, f0) ->
  let check_var = fun xi ->
    if is_glob xi.v_var
    then cferror (Ferr_remove_glob (Coq_xH, xi))
    else Ok ()
  in
  Result.bind (fun _ ->
    Result.bind (fun _ ->
      Result.bind (fun envc -> Ok (fn, { f_iinfo = f0.f_iinfo; f_tyin =
        f0.f_tyin; f_params = f0.f_params; f_body = (snd envc); f_tyout =
        f0.f_tyout; f_res = f0.f_res }))
        (remove_glob (remove_glob_i is_glob gd fn) Mvar.empty f0.f_body))
      (mapM check_var f0.f_res)) (mapM check_var f0.f_params)

(** val remove_glob_prog :
    (Var.var -> bool) -> (glob_decl list -> Var.var -> Equality.sort) -> prog
    -> (fun_error, prog) result **)

let remove_glob_prog is_glob fresh_id p =
  Result.bind (fun gd ->
    if uniq global_eqType (map (Obj.magic fst) gd)
    then Result.bind (fun fs -> Ok { p_globs = gd; p_funcs = fs })
           (mapM (remove_glob_fundef is_glob gd) p.p_funcs)
    else cferror Ferr_uniqglob) (extend_glob_prog is_glob fresh_id p)
