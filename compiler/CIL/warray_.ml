open BinInt
open BinNums
open Datatypes
open Eqtype
open Gen_map
open Memory_model
open Seq
open SsrZ
open Ssralg
open Ssrfun
open Utils0
open Word0
open Wsize

module WArray =
 struct
  type array =
    GRing.ComRing.sort Mz.t
    (* singleton inductive, whose constructor was Build_array *)

  (** val arr_data : positive -> array -> GRing.ComRing.sort Mz.t **)

  let arr_data _ a =
    a

  (** val empty : positive -> array **)

  let empty _ =
    Mz.empty

  (** val add : coq_Z -> coq_Z -> coq_Z **)

  let add =
    Z.add

  (** val sub : coq_Z -> coq_Z -> coq_Z **)

  let sub =
    Z.sub

  (** val uget : positive -> array -> Equality.sort -> GRing.Zmodule.sort **)

  let uget s m i =
    Option.default (GRing.zero (GRing.ComRing.zmodType (word U8)))
      (Mz.get (arr_data s m) i)

  (** val uset :
      positive -> array -> Equality.sort -> GRing.ComRing.sort -> array **)

  let uset s m i v =
    Mz.set (arr_data s m) i v

  (** val in_range : positive -> Equality.sort -> wsize -> bool **)

  let in_range s p ws =
    (&&) (Z.leb Z0 (Obj.magic p))
      (Z.leb (Z.add (Obj.magic p) (wsize_size ws)) (Zpos s))

  (** val validw :
      positive -> array -> Equality.sort -> wsize -> unit exec **)

  let validw s _ p ws =
    coq_assert (in_range s p ws) ErrOob

  (** val validr :
      positive -> array -> Equality.sort -> wsize -> (error, unit) result **)

  let validr s m p ws =
    Result.bind (fun _ ->
      coq_assert
        (all (fun i ->
          negb
            (eq_op (option_eqType (GRing.ComRing.eqType (word U8)))
              (Obj.magic Mz.get (arr_data s m) (Z.add (Obj.magic p) i))
              (Obj.magic None))) (ziota Z0 (wsize_size ws))) ErrAddrInvalid)
      (validw s m p ws)

  (** val array_CM : positive -> array coreMem **)

  let array_CM s =
    { Memory_model.add = (Obj.magic add); Memory_model.sub = (Obj.magic sub);
      Memory_model.uget = (uget s); Memory_model.uset = (uset s);
      Memory_model.validr = (validr s); Memory_model.validw = (validw s) }

  (** val get :
      positive -> wsize -> array -> coq_Z -> GRing.ComRing.sort exec **)

  let get len ws a p =
    CoreMem.read coq_Z_eqType (array_CM len) a
      (Obj.magic Z.mul p (wsize_size ws)) ws

  (** val set :
      positive -> wsize -> array -> coq_Z -> GRing.ComRing.sort -> array exec **)

  let set len ws a p v =
    CoreMem.write coq_Z_eqType (array_CM len) a
      (Obj.magic Z.mul p (wsize_size ws)) ws v

  (** val cast : positive -> positive -> array -> (error, array) result **)

  let cast len len' a =
    if Z.leb (Zpos len') (Zpos len) then Ok (arr_data len a) else type_error
 end
