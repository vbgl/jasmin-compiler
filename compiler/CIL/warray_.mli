open BinInt
open BinNums
open Datatypes
open Eqtype
open Gen_map
open Memory_model
open Seq
open SsrZ
open Ssralg
open Ssrfun
open Utils0
open Word0
open Wsize

module WArray :
 sig
  type array =
    GRing.ComRing.sort Mz.t
    (* singleton inductive, whose constructor was Build_array *)

  val arr_data : positive -> array -> GRing.ComRing.sort Mz.t

  val empty : positive -> array

  val add : coq_Z -> coq_Z -> coq_Z

  val sub : coq_Z -> coq_Z -> coq_Z

  val uget : positive -> array -> Equality.sort -> GRing.Zmodule.sort

  val uset : positive -> array -> Equality.sort -> GRing.ComRing.sort -> array

  val in_range : positive -> Equality.sort -> wsize -> bool

  val validw : positive -> array -> Equality.sort -> wsize -> unit exec

  val validr :
    positive -> array -> Equality.sort -> wsize -> (error, unit) result

  val array_CM : positive -> array coreMem

  val get : positive -> wsize -> array -> coq_Z -> GRing.ComRing.sort exec

  val set :
    positive -> wsize -> array -> coq_Z -> GRing.ComRing.sort -> array exec

  val cast : positive -> positive -> array -> (error, array) result
 end
